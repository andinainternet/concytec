<?php

// BEGIN iThemes Security - No modifiques ni borres esta línea
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Desactivar editor de archivos - Seguridad > Ajustes > Ajustes WordPress > Editor de archivos
// END iThemes Security - No modifiques ni borres esta línea

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'beta_concytec' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'u9ppzs0kphqgqw6gs14xylkwbqim7bgprdi2c4u4vv73uaxjwigaurbap5cquyzs' );
define( 'SECURE_AUTH_KEY',  'a0qn592tjzsrmfl9dqs3su7xfx5b3xld2jlaqakwmrz1jxi3kq1jobonlopimaw0' );
define( 'LOGGED_IN_KEY',    'yj6ozdfyniu0wjr8u4n9ufh3omcadob4wfy56jljzm3rso3m5hwpvnieurvtz9pf' );
define( 'NONCE_KEY',        'ku8zft2myahqmdtrs2b7f5dd0ok1b1iybd7jyyztxmkmjvyng51ljemegyl9lwzl' );
define( 'AUTH_SALT',        'foa7rpnnv1ntrg7krjacpukc7xk7zia5akbihrhpkak3ohnnml2nnp3wii39g8ou' );
define( 'SECURE_AUTH_SALT', '2yz3volck5jycjshybictkywkyz5zqxswa6nqvr38trh4qoo52c74wytwoycdsbr' );
define( 'LOGGED_IN_SALT',   'jjrdtnkrscbi2zufct8wndgp7vjfrod52vuhvqmipxvfkxmgmz6chlstdgjaet1e' );
define( 'NONCE_SALT',       'ywynmlzvtcmskyci4cdr4ucxzfxt2aznty0fqbkht90viayids99z1wbqas7yuhl' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpfd_concy2020_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
