# Concytec web page

Esta es el sitio web corporativo desarrollado para Concytec

## Herramientas

Se ha usado WordPress como capa de administración y un theme a medida conforme a los requerimientos del cliente.



## Consideraciones tecnicas

Es importante reemplazar el archivo `wp-config-sample.php` por `wp-config.php` y reemplazar la información interna de base de datos, usuario y contraseña.
Asi mismo necesitan identificar si usaran Apacha o Nginx como servidor web, en base a eso utilizar el archivo .htaccess (renombrar sample.htaccess por .htaccess)

Para mayor información sobre el archivo configurador se puede consultar el siguiente enlace oficial: https://wordpress.org/support/article/editing-wp-config-php/

Si necesitan modificar el tamaño maximo de subida de archivos pueden consultar el siguiente recurso https://kinsta.com/es/blog/aumentar-el-tamano-maximo-subidas/




