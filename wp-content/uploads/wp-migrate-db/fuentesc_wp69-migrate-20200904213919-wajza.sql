# Migración de la base de datos MySQL de WordPress
#
# Generado: Friday 4. September 2020 21:39 UTC
# Hostname: localhost
# Base de datos: `fuentesc_wp69`
# URL: //localhost/concytec
# Path: /home/fuentesc/public_html/proyectos/concytec
# Tables: wpfd_concy2020_commentmeta, wpfd_concy2020_comments, wpfd_concy2020_itsec_distributed_storage, wpfd_concy2020_itsec_fingerprints, wpfd_concy2020_itsec_geolocation_cache, wpfd_concy2020_itsec_lockouts, wpfd_concy2020_itsec_logs, wpfd_concy2020_itsec_mutexes, wpfd_concy2020_itsec_opaque_tokens, wpfd_concy2020_itsec_temp, wpfd_concy2020_itsec_user_groups, wpfd_concy2020_links, wpfd_concy2020_options, wpfd_concy2020_postmeta, wpfd_concy2020_posts, wpfd_concy2020_term_relationships, wpfd_concy2020_term_taxonomy, wpfd_concy2020_termmeta, wpfd_concy2020_terms, wpfd_concy2020_usermeta, wpfd_concy2020_users, wpfd_concy2020_wfblockediplog, wpfd_concy2020_wfblocks7, wpfd_concy2020_wfconfig, wpfd_concy2020_wfcrawlers, wpfd_concy2020_wffilechanges, wpfd_concy2020_wffilemods, wpfd_concy2020_wfhits, wpfd_concy2020_wfhoover, wpfd_concy2020_wfissues, wpfd_concy2020_wfknownfilelist, wpfd_concy2020_wflivetraffichuman, wpfd_concy2020_wflocs, wpfd_concy2020_wflogins, wpfd_concy2020_wfls_2fa_secrets, wpfd_concy2020_wfls_settings, wpfd_concy2020_wfnotifications, wpfd_concy2020_wfpendingissues, wpfd_concy2020_wfreversecache, wpfd_concy2020_wfsnipcache, wpfd_concy2020_wfstatus, wpfd_concy2020_wftrafficrates
# Table Prefix: wpfd_concy2020_
# Post Types: revision, page, post, wpcf7_contact_form
# Protocol: https
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Eliminar cualquier tabla existente `wpfd_concy2020_commentmeta`
#

DROP TABLE IF EXISTS `wpfd_concy2020_commentmeta`;


#
# Estructura de la tabla de la tabla `wpfd_concy2020_commentmeta`
#

CREATE TABLE `wpfd_concy2020_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Contenido de la tabla `wpfd_concy2020_commentmeta`
#

#
# Fin de los contenidos de datos de la tabla `wpfd_concy2020_commentmeta`
# --------------------------------------------------------



#
# Eliminar cualquier tabla existente `wpfd_concy2020_comments`
#

DROP TABLE IF EXISTS `wpfd_concy2020_comments`;


#
# Estructura de la tabla de la tabla `wpfd_concy2020_comments`
#

CREATE TABLE `wpfd_concy2020_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Contenido de la tabla `wpfd_concy2020_comments`
#
INSERT INTO `wpfd_concy2020_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-09-02 21:52:17', '2020-09-02 21:52:17', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', 'comment', 0, 0) ;

#
# Fin de los contenidos de datos de la tabla `wpfd_concy2020_comments`
# --------------------------------------------------------



#
# Eliminar cualquier tabla existente `wpfd_concy2020_itsec_distributed_storage`
#

DROP TABLE IF EXISTS `wpfd_concy2020_itsec_distributed_storage`;


#
# Estructura de la tabla de la tabla `wpfd_concy2020_itsec_distributed_storage`
#

CREATE TABLE `wpfd_concy2020_itsec_distributed_storage` (
  `storage_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `storage_group` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `storage_key` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storage_chunk` int(11) NOT NULL DEFAULT '0',
  `storage_data` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `storage_updated` datetime NOT NULL,
  PRIMARY KEY (`storage_id`),
  UNIQUE KEY `storage_group__key__chunk` (`storage_group`,`storage_key`,`storage_chunk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Contenido de la tabla `wpfd_concy2020_itsec_distributed_storage`
#

#
# Fin de los contenidos de datos de la tabla `wpfd_concy2020_itsec_distributed_storage`
# --------------------------------------------------------

