<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="author" content="<?php bloginfo('name'); ?>">
	<meta name="robots" content="all, index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" >
	
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/public/assets/css/main.min.css"/> -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/public/assets/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="main-header">
	<div class="main-header__all">
		<div class="main-header__top">
			<div class="main-header__topcontainer wrapper-container">
				<div class="main-header__logos">
					<a class="main-header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php the_field('logotipo_header','options') ?>" alt=""/>
					</a>
					<?php if(get_field('logotipos_header','options')): ?>
					<div class="main-header__logosList">
						<?php while(has_sub_field('logotipos_header','options')): ?>
							<?php if(get_sub_field('url_logotipos_header','options')): ?>
								<a class="main-header__logoList" href="<?php the_sub_field('url_logotipos_header','options') ?>" target="_blank">
									<img src="<?php the_sub_field('logotipo_logotipos_header','options') ?>" alt=""/>
								</a>
							<?php else: ?>
								<figure class="main-header__logoList">
									<img src="<?php the_sub_field('logotipo_logotipos_header','options') ?>" alt=""/>
								</figure>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="main-header__searchBurguer">
					<!-- Search Desktop -->
					<div class="main-header__search">
						<form id="search-g--prueba" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET" >
							<div class="input-g input-g--iconRight input-g--button main-header__searchIn">
								<div class="input-g__inside">
									<button class="input-g__ico input-g__icoRight icon-buscador"></button>
									<input class="input-g__input" name="s" type="text" placeholder="<?php _e('Buscar...','concytec')?>"/>
								</div>
							</div>
						</form>
					</div>
					<!-- -->
					<!-- Search Mobile -->
					<div class="main-header__iconSearch main-header--iconSearch icon-buscador"></div>
					<!-- -->
					<!-- Burguer -->
					<div class="burguer-g burguer-g--icon main-header__burguer main-header__burguerDesktop header-header--pitcher">
						<div class="burguer-g__icon icon-menu"></div>
					</div>
					<div class="burguer-g burguer-g--icon main-header__burguer main-header__burguerMobile header-header--pitchersidebar">
						<div class="burguer-g__icon icon-menu"></div>
					</div>
					<!-- -->
				</div>
			</div>
		</div> 
		<div class="main-header__bottom active">
			<div class="main-header__bottomcontainer wrapper-container">
				<nav class="main-header__nav list--none">
					<?php wp_nav_menu( array(
						'theme_location' => 'header',
						'menu_class' => 'main-header__list',
						'container' => '',
						'container_class' => '',
					)); ?>
				</nav>
			</div>
		</div>
		<!-- Search mobile -->
		<div class="main-header__search main-header__searchMobile">
			<form id="search-g--prueba" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET" >
				<div class="input-g input-g--iconRight input-g--button main-header__searchIn">
					<div class="input-g__inside">
						<button class="input-g__ico input-g__icoRight icon-buscador"></button>
						<input class="input-g__input" name="s" type="text" placeholder="<?php _e('Buscar...','concytec')?>"/>
					</div>
				</div>
			</form>
		</div>
		<!-- -->
	</div> 
</header>

<div class="main-sidebar">
	<div class="main-sidebar__container list--none">		
		<nav class="main-sidebar__nav">
			<?php wp_nav_menu( array(
				'theme_location' => 'header-sidebar',
				'menu_class' => 'main-sidebar__list',
				'container' => '',
				'container_class' => '',
			)); ?>
		</nav>
		<?php if(get_field('logotipos_header','options')): ?>
		<div class="main-sidebar__logos">
			<?php while(has_sub_field('logotipos_header','options')): ?>
				<?php if(get_sub_field('url_logotipos_header','options')): ?>
					<a class="main-sidebar__logosList" href="<?php the_sub_field('url_logotipos_header','options') ?>">
						<img src="<?php the_sub_field('logotipo_logotipos_header','options') ?>" alt=""/>
					</a>
				<?php else: ?>
					<figure class="main-sidebar__logosList">
						<img src="<?php the_sub_field('logotipo_logotipos_header','options') ?>" alt=""/>
					</figure>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</div>


<main class="main-content">