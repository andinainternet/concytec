<?php
get_header(); ?>

<section class="wrapper-padding wrapper-news">
	
	<div class="wrapper-container stylesAll-content single-content--news">
		<div class="title-g block-g__blogTitle aos-init aos-animate">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block"><?php the_title(); ?> </strong>
                        
                    </h3>
                    <h4 style="font-style: italic;color: #cecece;">Publicado el <?php echo get_the_date(); ?></h4>
                </div>
            </div>
		<br>
		<?php 
		if(have_posts()) : while(have_posts()) : the_post(); ?>
		
		<div class="text-content colorBlue">
			<?php the_content(); ?>
		</div>

		<!-- BUTTON SINGLE PAGE -->

	        <?php if(get_field('upload_file_doc') ):  ?> 
	        
	        <div class="title-g__buttons">
	            <div class="title-g__button">
	                <a class="button-g button-g--green g--uppercase" href="<?php echo wp_get_attachment_url( get_field( 'upload_file_doc' ) ); ?>" target="_blank">
	                    <span class="button-g__text"><?php echo 'Ver estudio completo'; ?></span>
	                </a>
	            </div>
	        </div>
	        
	        <?php endif; ?>
		

		<?php 
		endwhile; 
		wp_reset_query();
		endif;
		?>
	</div>

	<div class="wrapper-sidebar sidebar-new">

		<div class="sidebar-content">
			<div class="sidebar-title ">
				<h3 class="title-g__fz--20 fontXBold colorLightBlue2">Documentos Relacionadas</h3>
			</div>
			<div class="sidebar-posts">
				<?php 
				/**
				 * Setup query to show the ‘services’ post type with ‘8’ posts.
				 * Output the title with an excerpt.
				 */
				    $args = array(  
				        'post_type' => 'documento-de-trabajo',
				        'post_status' => 'publish',
				        'posts_per_page' => 5, 
				        'orderby' => 'date', 
				        'order' => 'DESC', 
				    );

				    $loop = new WP_Query( $args ); 
				        
				    while ( $loop->have_posts() ) : $loop->the_post(); ?>
		
						<div class="sidebar-item ">
		                    <a href="<?php echo the_permalink(); ?>">
							  <h4 class="titleText-g__fz--16 colorBlue"><?php echo get_the_title(); ?></h4>
						    </a>
						</div>				    
				    
				   <?php endwhile;

				    wp_reset_postdata(); 
				  ?>				
				
				<!-- START LOOP -->

				<!-- ENDS LOOP -->

				<div class="more-post titleText-g__fz--12 colorGreen">
					<a class="colorGreen" href="#!">Ver más Documentos</a>
				</div>

			</div>			
		</div>

	</div>

</section>

<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>
