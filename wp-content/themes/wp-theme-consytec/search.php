<?php
get_header();
global $wp_query;
?>
<div class="wapper">
  <div class="contentarea clearfix">
    <div class="content content-results">
      


        <?php if ( have_posts() ) { ?>

            <div class="container-results">

            <?php while ( have_posts() ) { the_post(); ?>

				<div class="item-result">
					
					<div class="item-col result-data">
						<?php 
						$postType = get_post_type_object(get_post_type());

						if ($postType) { ?>
							<h3 class="titleText-g__fz--18 fontXBold colorGreen"><?php echo esc_html($postType->labels->singular_name); ?></h3>
						<?php }	 ?>

						<p class="data--date"><?php echo get_the_date(); ?></p>
						
					</div>

					<div class="item-col result-image">
						<figure>
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						</figure>
					</div>

					<div class="item-col result-content">
						<h4 class="title-g__fz--25 fontXBold colorBlue title-g__block">
							<?php echo get_the_title(); ?></h4>
						<article>
							<p class="title-g__text colorBlue fontRegular titleText-g__fz--20">
								<?php echo substr(get_the_excerpt(), 0,500); ?>
							</p>
						</article>
					</div>

					<div class="item-col result-btn">
						<a class="button-g button-g--green g--uppercase" href="<?php echo get_permalink(); ?>">
                                <span class="titleText-g__fz--16 button-g__text btn-results">Ver <?php 
                                if ($postType) { 
                                	echo trim(esc_html($postType->labels->singular_name)); 
                                }?></span>
                            </a>						
					</div>

				</div>                   

            <?php } ?>



            </div>
            <br>
	        <br>
   	        <div class="block-g__listPagination">
	            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
	        </div>
	        <br>
	        <br>
	        <br>
	        <br>
        <?php } else { ?>

        	<div class="content-notfound">
        		
        		<figure>
        			<img src="./wp-content/themes/wp-theme-consytec/public/assets/images/error-search.svg" alt="">
        		</figure>

        		<h3 class="titleText-g__fz--22 fontBold colorGray"> Sin resultados de búsqueda</h3>

        		<p class="colorGray">Intenta con una nueva palabra o visita las categorías de la web navegando por el menú principal.</p>


        	</div>

        <?php } ?>

    </div>
  </div>
</div>
<?php get_footer(); ?>