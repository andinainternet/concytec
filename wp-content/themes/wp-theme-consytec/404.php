<?php get_header(); ?>

<div class="message-g message-g--404 message-g--default">
	<div class="message-g__container wrapper-container">
		<div class="message-g__image">
			<img src="<?php the_field('imagen_404','options') ?>" alt="">
		</div>
		<div class="message-g__title">
			<h1><?php the_field('titulo_404','options') ?></h1>
		</div>
		<div class="message-g__text">
			<p><?php the_field('texto_404','options') ?></p>
		</div>
		<div class="message-g__button">
			<a class="button-g button-g--green g--uppercase" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<span class="button-g__text"><?php _e('Volver al inicio','concytec'); ?></span>
			</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>