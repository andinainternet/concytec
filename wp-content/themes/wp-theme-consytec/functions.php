<?php
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'    => 'Configuracion General',
		'menu_title'    => 'Configuraciones',
		'menu_slug'     => 'theme-general-settings',
		'capability'    => 'edit_posts',
		'redirect'      => false,
		'position'      => '2.1',
		'icon_url'      => 'dashicons-admin-settings',
	));
}

// Remove action
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');

// Remove Emojis
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}
	return $urls;
}

// Remove embeds
function disable_embeds_code_init() {
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	add_filter( 'embed_oembed_discover', '__return_false' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	add_filter( 'tiny_mce_plugins', 'disable_embeds_tiny_mce_plugin' );
	add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
	remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
}
add_action( 'init', 'disable_embeds_code_init', 9999 );

function disable_embeds_tiny_mce_plugin($plugins) {
	return array_diff($plugins, array('wpembed'));
}

function disable_embeds_rewrites($rules) {
	foreach($rules as $rule => $rewrite) {
	if(false !== strpos($rewrite, 'embed=true')) {
		unset($rules[$rule]);
	}
	}
	return $rules;
}

// scripts
add_action( 'wp_enqueue_scripts', 'sd__custom__style__script' );
function sd__custom__style__script(){

	wp_register_script( 'main-js', get_stylesheet_directory_uri() . '/public/assets/js/main.min.js');
	wp_enqueue_script('main-js');

	// // gmaps
	// $maps_apikey = get_field('apikey_gmaps', 'options');
	$maps_apikey = get_field('api_key_gmaps','options');
	//define('GMAPS_API', ''. $maps_apikey . '');
	define('GMAPS_API', ''. $maps_apikey . '');
	
	wp_register_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' . GMAPS_API );
	wp_enqueue_script('google-maps-api');
}


//Language
load_theme_textdomain('concytec', get_template_directory() . '/languages');

//Image Sizes
add_image_size( 'thumbnail-size', '1024', '450', array( "1", "") );
add_image_size( 'image-blog', '408', '230', array( "1", "") );
add_image_size( 'image-blog-inicio', '363', '182', array( "1", "") );


//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
	$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Support SVG
add_filter('upload_mimes', 'suppot_upload_svg');
function suppot_upload_svg($mimes = array()) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

// Register menu
register_nav_menus(array(
	'header'           => __('Menu Header', ''),
	'header-sidebar'           => __('Menu Sidebar', ''),
	'footer-fondecyt'            => __('Menu Footer Fondecyt', ''),
	'footer-convocatorios'            => __('Menu Footer Convocatorias', ''),
	'footer-comunicaciones'            => __('Menu Footer Comunicaciones', ''),
	'footer-enlaces'            => __('Menu Footer Enlaces', ''),
));

// Remove columns in admin pages
add_filter( 'manage_pages_columns', 'custom_pages_columns' );
function custom_pages_columns( $columns ) {
	$subtitle = array( 'subtitle' => $columns['subtitle'] );
	unset(
		$columns['subtitle'],
		$columns['author'],
		$columns['tags'],
		$columns['comments'],
		$columns['language'],
		$columns['categories']
);
return $columns;
}

// Custom format date post created
function the_date_custom_post() {
	$dateDay    = ''.get_the_date('j').'';
	$dateMonth  = ''.get_the_date('M').'';
	$dateYear   = ''.get_the_date('Y').''; ?>
	<?php echo $dateDay; ?> <?php echo $dateMonth; ?>. <?php echo $dateYear; ?>
	<?php
}

//customize the PageNavi HTML before it is output
add_filter( 'wp_pagenavi', 'wd_pagination', 10, 2 );
function wd_pagination($html) {
	$out = '';
	$out = str_replace("<a","<li><a",$html);    
	$out = str_replace("</a>","</a></li>",$out);
	$out = str_replace("<span","<li><span",$out);   
	$out = str_replace("</span>","</span></li>",$out);
	$out = str_replace("<div class='wp-pagenavi'>","",$out);
	$out = str_replace("</div>","",$out);
	return '<div class="pagination">
			<ul>'.$out.'</ul>
		</div>';
}

// Custom excerpt
function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '';
	} else {
		echo $excerpt;
	}
}

// Support Post Thumbnail
add_theme_support( 'post-thumbnails' );

function time_ago( $type = 'post' ) {
    $d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
    return  __('Hace') . " " .human_time_diff($d('U'), current_time('timestamp'));
}

// Support Tiles
add_theme_support( 'title-tag' );

// Redirect page CF7
function add_this_script_footer(){ ?>
	<script>
		document.addEventListener( 'wpcf7mailsent', function( event ) {  
			location = '<?php echo esc_url( home_url( '' ) ); ?>/mensaje-enviado';
		}, false );
	</script> 
<?php 
}  
add_action('wp_footer', 'add_this_script_footer'); 

// Remove admin
function remove_menus(){
	remove_menu_page( 'edit-comments.php' );

}
add_action( 'admin_menu', 'remove_menus' );

// social media
function wp_custom_social_media($content = null) {
	ob_start(); ?>
	<?php if( have_rows('list_social_media', 'options') ): ?>
		<ul>
			<?php while( have_rows('list_social_media', 'options') ): the_row(); ?>
			<?php 
				$name_select_sub_field = (get_sub_field_object('tipo_social_media'));
				$name_sub_field = get_sub_field('tipo_social_media');
				$label_select = ($name_select_sub_field['choices'][$name_sub_field]);
			?>
			<?php if( $name_sub_field == 'whatsapp' ) { ?>
			<li>
				<a  class="icon-<?php the_sub_field( 'tipo_social_media' ); ?>" target="_blank" href="https://api.whatsapp.com/send?phone=<?php the_field( 'celular', 'options' ); ?>&text=<?php _e('Conversa con Concyssa','concyssa') ?>">
				</a>
			</li>
			<?php } elseif( $name_sub_field != 'whatsapp' ) {  ?>
				<?php if( get_sub_field('url_social_media', 'options') ) { ?>
			<li>
				<a class="icon-<?php the_sub_field( 'tipo_social_media' ); ?>" href="<?php the_sub_field('url_social_media', 'options'); ?>" target="_blank"> </a>
			</li>
			<?php } ?>
			<?php } ?>
			<?php endwhile; ?>
		</ul>
		<?php endif;
	$content = ob_get_contents();
	ob_end_clean();
	return $content;  
}

add_shortcode('social_media_list', 'wp_custom_social_media');


add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;
	$home = get_the_title($post_id);
	$publicaciones = get_the_title($post_id);
	$noticias = get_the_title($post_id);
	$contacto = get_the_title($post_id);
	$equipamientosl = get_the_title($post_id);
	$equipamientos = get_the_title($post_id);
	$investigaciones = get_the_title($post_id);
	$investigacionesl = get_the_title($post_id);
	$programasl = get_the_title($post_id);
	$programas = get_the_title($post_id);
	if($home == 'Inicio' || $publicaciones == 'Publicaciones' || $noticias == 'Noticias' || $contacto == 'Contacto' || $equipamientosl == 'Equipamientos listado' || $equipamientos == 'Equipamientos' || $investigaciones == 'Investigaciones' || $investigacionesl == 'Investigaciones Listado' || $programas == 'Programas Listado' || $programasl == 'Programas'){ 
	remove_post_type_support('page', 'editor');
	}
}


function tutsplus_widgets_init() {

	// First footer widget area, located in the footer. Empty by default.
	register_sidebar( array(
        'name' => __( 'Footer Seccion #1', 'concytec' ),
        'id' => 'first-footer-widget-area',
        'description' => __( 'Primer Widget', 'concytec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Footer Seccion #2', 'concytec' ),
        'id' => 'second-footer-widget-area',
        'description' => __( 'Segundo Widget', 'concytec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Footer Seccion #3', 'concytec' ),
        'id' => 'third-footer-widget-area',
        'description' => __( 'Tercer Widget', 'concytec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Footer Seccion #4', 'concytec' ),
        'id' => 'fourth-footer-widget-area',
        'description' => __( 'Cuarto Widget', 'concytec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

}

// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action( 'widgets_init', 'tutsplus_widgets_init' );



/***
 * ANDINA PATCH
 ***/

function sidebar_noticias() {
    register_sidebar(
        array (
            'name' => __( 'Noticias Relacionadas', 'wp-theme-consytec' ),
            'id' => 'sidebar-news',
            'description' => __( 'Noticias Relacionadas', 'wp-theme-consytec' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'sidebar_noticias' );


/**Limit string**/
function string_limit($string, $limit, $suf){
	// Si la longitud es mayor que el límite...
	if(strlen($string) > $limit){
		// Entonces corta la string y ponle el suf
		return substr($string, 0, $limit) . $suf;
	}
	
	// Si no, entonces devuelve la string normal
	return $string;
}




function wporg_add_custom_box() {
    $screens = [ 'page', 'wporg_cpt' ];
    foreach ( $screens as $screen ) {
        add_meta_box(
            'wporg_box_id',                 // Unique ID
            'Selecciona el custom post type',      // Box title
            'wporg_custom_box_html',  // Content callback, must be of type callable
            $screen                            // Post type
        );
    }
}

// global $post;
   $post_id = $_GET['post'];

$pageTemplate = get_post_meta( $post_id, '_wp_page_template', true);

// var_dump($pageTemplate);

if ($pageTemplate == 'templates/theme-grid-post.php') {
	add_action( 'add_meta_boxes', 'wporg_add_custom_box' );
}





function wporg_custom_box_html( $post ) {

	$args = array(
		    'public'   => true,
		    '_builtin' => false,
		  );

	    $output = 'names'; // names or objects, note names is the default
	    $operator = 'and'; // 'and' or 'or'
		$post_types = get_post_types( $args, $operator ); 
		$postTypeArray = [];
		$value = get_post_meta( $post->ID, '_wporg_meta_key', true );

		$taxonomies = get_object_taxonomies($value);

		$terms = get_terms([
		    'taxonomy' => $taxonomies[0],
		    'hide_empty' => false,
		]);

		// echo '<pre>';
		// var_dump( $taxonomies );
		// echo '</pre>';

    ?>
    


    <h4>Selecciona el Custom Post Type</h4>

    <input name="wporg_field_cat" type="hidden" value="<?php echo $taxonomies[0]; ?>">
    
    <select name="wporg_field" id="wporg_field" class="postbox">
        
		<?php 

	 	// var_dump($value);

		 foreach ( $post_types  as $post_type ) { 

		 	?>

		 	<?php if ($value == $post_type->rewrite['slug']): ?>

		 	<option value="<?php echo $value; ?>" selected> <?php echo $post_type->label; ?> </option>

		 	<?php else: ?>
	        
	        <option value="<?php echo $post_type->name ?>"> <?php echo $post_type->label; ?> </option>

		 	<?php endif ?>


	 	<?php } ?>

    </select>


    <?php if ($terms): ?>

    	<?php 
    		
  //   			echo '<pre>';
		// var_dump(  get_post_meta( $post->ID, 'wporg_field_cat', true ) );
		// echo '</pre>';

 ?>
	<h4>Selecciona la categoría interna <em>(guarda primero)</em></h4>

	 <select name="wporg_field_taxonomy" id="wporg_field_taxonomy" class="postbox">
	        
			<?php 
		 	
		 	$value_tax = get_post_meta( $post->ID, 'wporg_field_taxonomy', true ); ?>

		 	<option value="null"><?php echo 'Todos'; ?> </option>
		 	<?php 
			 foreach ( $terms as $term ) { 

			 	?>

			 	<?php if ($value_tax == $term->slug): ?>

			 	<option value="<?php echo $value_tax; ?>" selected> <?php echo $term->name; ?> </option>

			 	<?php else: ?>
		        
		        <option value="<?php echo $term->slug ?>"> <?php echo $term->name; ?> </option>

			 	<?php endif ?>


		 	<?php } ?>

	    </select>   

   <?php endif ?> 


    <?php
}


function wporg_save_postdata( $post_id ) {
    if ( array_key_exists( 'wporg_field', $_POST ) ) {
        
        update_post_meta(
            $post_id,
            '_wporg_meta_key',
            $_POST['wporg_field']
        );

		 update_post_meta(
            $post_id,
            'wporg_field_taxonomy',
            $_POST['wporg_field_taxonomy']
        );

		 update_post_meta(
            $post_id,
            'wporg_field_cat',
            $_POST['wporg_field_cat']
        );

    }
}
add_action( 'save_post', 'wporg_save_postdata' );
/***/





?>