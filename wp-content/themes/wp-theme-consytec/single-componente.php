<?php get_header(); ?>

<!-- Investigacion Detail-->

<?php if( have_rows('componente_contenido') ): ?>
<?php while( have_rows('componente_contenido') ): the_row(); ?>
<?php if( get_row_layout() == 'baner_principal' ): ?>

<?php elseif( get_row_layout() == 'baner' ):  ?>
<section class="block-g component-block--banner" style="background-color: <?php the_sub_field('background_color_equipamiento_pagina1')?>">
    
    <div class="block-g__container wrapper-container">  
        
        <div class="title-g block-g__title">
            <div class="title-g__title">
            	<?php if(get_sub_field('subtitulo_equipamiento_pagina1') ):  ?>
            		<h4 class="title-g__fz--37 fontXBold title-g__block g--uppercase" style="color:<?php the_sub_field('subtituloc_equipamiento_pagina1')?>">
            			<?php the_sub_field('subtitulo_equipamiento_pagina1')?>
            		</h4>
            	<?php endif; ?>
            	<?php if(get_sub_field('titulo_equipamiento_pagina1') ):  ?>
            		 <?php
                    $componentSlider_title = get_sub_field('titulo_equipamiento_pagina1');
                    $componentSlider_titleArray = preg_split( "/[|]/", $componentSlider_title );
                    $componentSlider_array1 = $componentSlider_titleArray[0];
                    $componentSlider_array2 = $componentSlider_titleArray[1];
                    ?>	
	            	<h1 class="g--uppercase" style="color:<?php the_sub_field('tituloc_equipamiento_pagina1')?>">
	            		<span class="title-g__fz--64 fontBold title-g__block"><?php echo $componentSlider_array1; ?></span>
	            		<strong class="title-g__fz--80 fontXBold title-g__block"><?php echo $componentSlider_array2; ?></strong>
	            	</h1>
            	<?php endif; ?>
            </div>
            <?php if(get_sub_field('texto_equipamiento_pagina1') ):  ?>
            <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                <?php the_sub_field('texto_equipamiento_pagina1') ?>
            </div>
            <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_equipamiento_pagina1')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_equipamiento_pagina1') ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section> 

<?php elseif( get_row_layout() == 'objetivo_presupuesto' ):  ?>

<?php if(get_sub_field('link_equipamiento_pagina2')) : ?>
<section class="component-objpre" style="align-items: flex-end;">
<?php else: ?>
<section class="component-objpre" style="align-items: center;">
<?php endif; ?>

	<div class="component-objpre__container wrapper-container">
		<div class="component-objpre__items">

            <article class="component-objpre__item" >
				<span class="component-objpre__figure fig1"></span>
				<div class="component-objpre__itemTitle g--uppercase">
					<h3><?php the_sub_field('titulo_equipamiento_pagina2') ?></h3>
				</div>
				<div class="component-objpre__itemText">
					<?php the_sub_field('texto_equipamiento_pagina2') ?>
				</div>
			</article>


			<article class="component-objpre__item">
				<span class="component-objpre__figure fig2"></span>
				<div class="component-objpre__itemTitle g--uppercase">
					<h3><?php the_sub_field('titulo2_equipamiento_pagina2') ?></h3>
				</div>
				<div class="component-objpre__itemText itemText-left">
					<?php the_sub_field('texto2_equipamiento_pagina2') ?>
				</div>
				<div class="component-objpre__itemPrice">
					<?php the_sub_field('monto_equipamiento_pagina2') ?>
				</div>
			</article>


		</div>
	</div>

    <?php if(get_sub_field('link_equipamiento_pagina2')) : ?>
        echo         <div class="btn--float-component title-g__buttons">
            <div class="title-g__button">
                <a class="button-g button-g--lightBlue2 g--uppercase" target="_blank" href="<?php echo get_sub_field( 'link_equipamiento_pagina2' ); ?>">
                    <span class="button-g__text"><?php echo get_sub_field( 'text_equipamiento_pagina2' ); ?></span>
                </a>
            </div>
        </div>
    
    <?php endif; ?>

</section>



<!-- GASTO PUBLICO -->

<?php elseif( get_row_layout() == 'gasto_publico' ):  ?>

<!-- <section class="component-objpre"> -->
    <!-- <div class="component-objpre__container wrapper-container"> -->
<section class="block-g equipamientos-block--equipos">
    <div class="block-g__container wrapper-container">  

        <div class="block-g__container wrapper-container">  
            
                <div class="title-g block-g__title">
                    <div class="title-g__title">
                        <?php
                        $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina3');
                        $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                        $blockcomp1_array1 = $blockcomp1_titleArray[0];
                        $blockcomp1_array2 = $blockcomp1_titleArray[1];
                        ?>
                        <h2 class="g--uppercase">
                            <span class="title-g__fz--64 fontBold colorWhite"><?php echo $blockcomp1_array1?></span>
                            <strong class="title-g__fz--80 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>
                        </h2>
                    </div>
                        <?php if(get_sub_field('titulo_equipamiento_pagina3') ):  ?>
                        <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                            <?php the_sub_field('texto_equipamiento_pagina3') ?>
                        </div>
                        <?php endif; ?>
                        
                        <?php if(get_sub_field('boton_url_equipamiento_pagina3') ):  ?>
                            <div class="title-g__buttons">
                                <div class="title-g__button">
                                    <a class="button-g button-g--lightBlue2 g--uppercase" href="<?php the_sub_field('boton_url_equipamiento_pagina3') ?>">
                                        <span class="button-g__text"><?php the_sub_field('boton_label_equipamiento_pagina3'); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>                        
                </div>

                <?php if(get_sub_field('imagen_equipamiento_pagina3')) :?>
                
                <div class="block-g__image">
                    <div class="block-g__figure"></div>
                    <figure class="block-g__img">
                        <img src="<?php the_sub_field('imagen_equipamiento_pagina3') ?>" alt="">
                    </figure>
                </div>
                
                <?php endif; ?>
        </div>

    </div>
</section>



<!-- GASTO PUBLICO -->

<?php elseif( get_row_layout() == 'conocimiento' ):  ?>
   <section class="block-g equipamientos-block--equipamientos">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title">
            <div class="title-g__title">
                <?php
                $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina4');
                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--64 fontXBold colorLightBlue2"><?php echo $blockcomp1_array1?></span>
                    <strong class="title-g__fz--80 fontXBold colorLightBlue2 title-g__block"><?php echo $blockcomp1_array2?></strong>
                    
                </h2>
            </div>
                <?php if(get_sub_field('texto_equipamiento_pagina4') ):  ?>
                <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina4') ?>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('boton_url_equipamiento_pagina4') ):  ?>
                <div class="title-g__buttons">
                    <div class="title-g__button">
                        <a class="button-g button-g--lightBlue2 g--uppercase" href="<?php the_sub_field('boton_url_equipamiento_pagina4') ?>">
                            <span class="button-g__text"><?php the_sub_field('boton_label_equipamiento_pagina4') ?></span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_equipamiento_pagina4')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_equipamiento_pagina4') ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section> 







<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>



<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>