<?php get_header(); ?>

<!-- Investigacion Detail-->

<?php 
if(have_posts()) : while(have_posts()) : the_post(); ?>


<?php if(get_field('contenido_detalleg')): ?>

<section class="detail-g">
    <div class="detail-g__container wrapper-container">
        <a href="#" onclick="history.go(-1);return false;"  class="detail-g__back"><?php _e('Atras','concytec')?></a>
        <div class="detail-g__tab tab-g tab--g">
            <div class="detail-g__tabLinks tab-g__links list--none tab--gLinks">
                <ul>
                <?php while(has_sub_field('contenido_detalleg')): ?>
                    <li class="detail-g__tabLink tab--gLink"><?php the_sub_field('titulo_contenido_detalleg') ?></li>
                <?php endwhile; ?>
                </ul>
            </div>
            
            <div class="detail-g__content list--none tab--gContent">
                <ul>
                <?php while(has_sub_field('contenido_detalleg')): ?>
                    <li class="tab--gContentItem">
                        <?php if(get_sub_field('imagen_contenido_detalleg')): ?>
                            <figure class="detail-g__contentImg">
                                <img src="<?php the_sub_field('imagen_contenido_detalleg') ?>" alt="">
                            </figure>
                        <?php endif; ?>
                        <div class="detail-g__contenttitle">
                            <h1><?php the_title() ?></h1>
                            <?php if(get_field('registro_detalleg')): ?>
                                <h3><?php the_field('registro_detalleg') ?></h3>
                            <?php endif; ?>
                            <?php if(get_field('subtitulo_detalleg')): ?>
                                <span><?php the_field('subtitulo_detalleg') ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="detail-g__contentInfoTitle">
                            <h2><?php the_sub_field('titulo_contenido_detalleg') ?></h2>
                        </div>
                        <?php if(get_sub_field('contenido_contenido_detalleg')): ?>
                        <div class="detail-g__contentInfo">
                            <?php the_sub_field('contenido_contenido_detalleg') ?>
                        </div>
                        <?php endif; ?>
                    </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
        <div class="detail-g__contentButton">
             <button class="button-g button-g--lightBlue2 button-g--withIcon g--uppercase ancla--js--top">
                <span class="button-g__text"><?php _e('Volver Arriba','concytec')?></span>
                <span class="button-g__icon icon-arrow-left2"></span>
            </button>
        </div>
    </div>
</section>

<?php endif; ?>

<?php 
endwhile; 
wp_reset_query();
endif;
?>


<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>