<?php
get_header(); ?>

<section class="wrapper-padding">
	<div class="wrapper-container stylesAll-content">
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block"><?php the_title(); ?> </strong>
                        
                    </h3>
                </div>
            </div>
		<br>
		<?php 
		if(have_posts()) : while(have_posts()) : the_post(); ?>
		
		<div class="text-content colorBlue">
			<?php the_content(); ?>
		</div>
		
		<?php 
		endwhile; 
		wp_reset_query();
		endif;
		?>
	</div>
</section>



<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>