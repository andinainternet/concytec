<?php
get_header(); ?>

<?php 

// function that runs when shortcode is called
function getTable ($table, $color, $clase ) {

	if ( ! empty ( $table ) ) {
	   echo '<div class="equipamiento-block__infTable single-table ' . $clase . '" >';
	        echo '<div class="table-g">';
		        echo '<table border="0">';
		            if ( ! empty( $table['caption'] ) ) {
		                echo '<caption>' . $table['caption'] . '</caption>';
		            }
		            if ( ! empty( $table['header'] ) ) {
		                echo '<thead class="thead-' . $color . '">';
		                    echo '<tr>';
		                        foreach ( $table['header'] as $th ) {
		                            echo '<th>';
		                                echo $th['c'];
		                            echo '</th>';
		                        }
		                    echo '</tr>';
		                echo '</thead>';
		            }
		            echo '<tbody>';
		                foreach ( $table['body'] as $tr ) {
		                    echo '<tr>';
		                        foreach ( $tr as $td ) {
		                            echo '<td>';
		                                echo $td['c'];
		                            echo '</td>';
		                        }
		                    echo '</tr>';
		                }
		            echo '</tbody>';
		        echo '</table>';
	        echo '</div>';
	    echo '</div>';

	}    
}

function wpb_demo_shortcode_1() {
ob_start(); 
	// Check value exists.
	if( have_rows('single_page_table_1') ):
		// Loop through rows.
		while ( have_rows('single_page_table_1') ) : the_row();
		    // Case: Paragraph layout.
		    if( get_row_layout() == 'single_table' ):
		        $table = get_sub_field('single_page_table_1');
		        $color = get_sub_field('color_table_1');
		        if (get_sub_field( 'campo_unico' )[0] == 'si') {
		        	$class = 'table-one';
		        } else {
		        	$class = '';
		        }


				getTable($table, $color, $class);			        
		    endif;
		// End loop.
		endwhile;
		wp_reset_query();
	// No value.
	else :
	// Do something...
	endif;
	return ob_get_clean();
} 


function wpb_demo_shortcode_2() { 
	ob_start();
	// Check value exists.
	if( have_rows('single_page_table_2') ):
		// Loop through rows.
		while ( have_rows('single_page_table_2') ) : the_row();
		    // Case: Paragraph layout.
		    if( get_row_layout() == 'single_table_2' ):
		        $table_2 = get_sub_field('single_page_table_2');
		        $color = get_sub_field('color_table_2');
				getTable($table_2, $color, 'tabs');		        
		    endif;
		// End loop.
		endwhile;
		wp_reset_query();
	// No value.
	else :
	// Do something...
	endif;
	return ob_get_clean();
} 

add_shortcode('single_table_1', 'wpb_demo_shortcode_1'); 
add_shortcode('single_table_2', 'wpb_demo_shortcode_2'); 


 ?>

<section class="wrapper-padding">
	<div class="wrapper-container stylesAll-content single-content">
		
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block"><?php the_title(); ?> </strong>
                        
                    </h3>
                </div>
            </div>
		<br>
		<?php 
		if(have_posts()) : while(have_posts()) : the_post(); ?>
		
		<div class="text-content colorBlue">
			<?php the_content(); ?>	
		</div>
		
		<?php 
		endwhile; 
		wp_reset_query();
		endif;
		?>

		<!-- button here -->

		<?php if(get_field('single_page_btn_link')) : ?>
	    <br/>
	    <br/>	
	    <div class="title-g__buttons" style="text-align: center;">
	        <div class="title-g__button">
	            <a class="button-g button-g--lightBlue g--uppercase" href="<?php echo get_field( 'single_page_btn_link' ); ?>" target="_blank">
	                <span class="button-g__text"><?php echo get_field( 'single_page_btn_txt' ); ?></span>
	            </a>
	        </div>
	    </div>
	    
	    <?php endif; ?>


	</div>
</section>




<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>