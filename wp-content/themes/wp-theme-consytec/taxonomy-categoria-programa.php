<?php get_header(); ?>

<!-- Programas Banner-->
<?php get_template_part( 'templates/programas/content', 'banner'); ?>

<!-- Programas Listado-->
<section class="block-g__list">
    <div class="block-g__listContainer wrapper-container">
        <div class="block-g__listNav">
            <?php get_template_part( 'templates/programas/content', 'menu'); ?>
        </div>
        <div class="block-g__listTitle g--uppercase">
            <?php $taxonomy = get_queried_object();?>
            <h2><?php echo  $taxonomy->name; ?></h2>
        </div>
        <?php if($wp_query->have_posts()) : ?>
        <div class="block-g__listItems">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <article class="block-g__listItem">
                    <?php get_template_part( 'templates/programas/content', 'investigacion'); ?>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        <div class="block-g__listPagination">
            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
        </div>
        <?php endif; ?>
    </div>   
</section>

<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>