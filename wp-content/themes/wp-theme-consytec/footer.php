</main>

<footer class="main-footer"> 
	<div class="main-footer__top">
		<div class="main-footer__topContainer wrapper-container">
			<div class="main-footer__items">
				<article class="main-footer__item list--none">
					<?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
					<?php endif; ?>
				</article>
				<article class="main-footer__item list--none">
					<?php if ( is_active_sidebar( 'second-footer-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
					<?php endif; ?>
				</article>
				<article class="main-footer__item list--none">
					<?php if ( is_active_sidebar( 'third-footer-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
					<?php endif; ?>
				</article>
				<article class="main-footer__item list--none">
					<?php if ( is_active_sidebar( 'fourth-footer-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
					<?php endif; ?>
				</article>
			</div>
			<div class="main-footer__social list--none">

				<div class="main-footer_socialData">
					
					<p><?php 
							echo '<span>Calle Chinchón N° 867 - San Isidro, Lima - Perú</span>';
						?>
						<span class="hideSep">|</span> 

						<span class="email-footer brJump">
							<br/>
							<?php echo get_field('correo','options'); ?>
						</span>
						
						<span class="hideSep">|</span> 

						<span class="brJump">
							<br/>
							<?php if( have_rows('central_telefonica','options') ): ?>
	                            
	                            <?php while( have_rows('central_telefonica','options') ): the_row(); ?>
	                            
	                                    <a href="tel:<?php the_sub_field('numero_central_telefonica','options') ?>"><?php the_sub_field('numero_central_telefonica','options') ?></a>
	                            <?php endwhile; ?>
	                            
	                        <?php endif; ?>
						</span>
					</p>

				</div>

				<div class="main-footer__socialForms">
					<a href="<?php echo get_home_url(); ?>/escribenos">CONSULTAS</a>

					<a href="<?php echo get_home_url(); ?>/reclamos">RECLAMOS</a>
				</div>


			<?php if( have_rows('lista_redes_sociales', 'options') ): ?>
				<ul class="main-footer__socialMedia">
					<?php while( have_rows('lista_redes_sociales', 'options') ): the_row(); ?>
						<?php 
							$name_select_sub_field = (get_sub_field_object('red_social_lista_redes_sociales'));
							$name_sub_field = get_sub_field('red_social_lista_redes_sociales');
							$label_select = ($name_select_sub_field['choices'][$name_sub_field]);
						?>
						
						<?php if( get_sub_field('url_lista_redes_sociales', 'options') ) { ?>
						<li>
						<a class="icon-<?php the_sub_field( 'red_social_lista_redes_sociales' ); ?>" href="<?php the_sub_field('url_lista_redes_sociales', 'options'); ?>" target="_blank"> </a>
						</li>
						<?php } ?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>	
			</div>
		</div>
	</div>
	<?php if(get_field('logotipos_footer','options')): ?>
		<div class="main-footer__logosList">
			<div class="main-footer__logosListContainer">
				<?php while(has_sub_field('logotipos_footer','options')): ?>
					<?php if(get_sub_field('url_logotipos_footer','options')): ?>
						<a class="main-footer__logoList" href="<?php the_sub_field('url_logotipos_footer','options') ?>" target="_blank">
							<img src="<?php the_sub_field('logotipo_logotipos_footer','options') ?>" alt=""/>
						</a>
					<?php else: ?>
						<figure class="main-footer__logoList">
							<img src="<?php the_sub_field('logotipo_logotipos_footer','options') ?>" alt=""/>
						</figure>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
</footer>

<style>

    .detail-g__contentInfo table{
        border: solid 1px #ccc;
        width: 100%;
    }
    .detail-g__contentInfo table tbody tr{
        border: solid 1px #ccc;
    }
    .detail-g__contentInfo table tbody tr td{
        /*background: #005470;*/
        border: solid 1px #ccc;
        padding: 10px 20px;
    }
</style>

<?php wp_footer(); ?>
</body>
</html>