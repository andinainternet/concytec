<?php
get_header(); ?>

<section class="wrapper-padding">
	<div class="wrapper-container stylesAll-content">
		<?php 
		if(have_posts()) : while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		
		<?php 
		endwhile; 
		wp_reset_query();
		endif;
		?>
	</div>
</section>


<?php get_footer(); ?>