<?php
/* Template Name: Template - Manual de identidad */
get_header(); ?>


<section class="header-single">
	<div class="bg-single bg-single-video" style="background-image: url(<?php echo get_field( 'banner_identidad' ); ?>)">
		<h1 class="titleSingle title-g__fz--55 fontXBold title-g__block"><?php echo get_the_title(); ?></h1>
	</div>
</section>



<section class="wrapper-padding">
	
	<div class="wrapper-container stylesAll-content">
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up" style="border: none;">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block">Archivos descargables</strong>
                        
                    </h3>
                </div>
            </div>
		<br>

		<div class="wrapper-grid">
			<div class="consy-row">
			
				<!-- start loop -->
			<?php 
				/**
				 * Setup query to show the ‘services’ post type with ‘8’ posts.
				 * Output the title with an excerpt.
				 */
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				    $args = array(  
				        'post_type' => 'documento-de-trabajo',
				        // 'category_name' => 'manuales-de-identidad',
						'tax_query' =>  array(
    									'relation' => 'AND',array(
								            'taxonomy' => 'categoria_documento', // Taxonomy, in my case I need default post categories
								            'field'    => 'slug',
								            'terms'    => 'manuales-de-identidad', // Your category slug (I have a category 'interior')
							        		)
										),				        
				        'post_status' => 'publish',
				        'posts_per_page' => 10, 
				        'orderby' => 'date', 
				        'order' => 'DESC', 
				        'paged' => $paged
				    );

				    $wp_query = new WP_Query( $args ); 

				    // echo '<pre>';
				    // var_dump($wp_query);
				    // echo '</pre>';
				        	
				    while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					
					<?php 

						$attachment_id = get_field('upload_file_doc');
					
						$url = wp_get_attachment_url( $attachment_id );
						$title = get_the_title( $attachment_id );
						// part where to get the filesize
						$filesize = filesize( get_attached_file( $attachment_id ) );
						$filesize = size_format($filesize, 2);
					?>


					<div class="consy-col consy-col-lg-12 consy-col-12">
						
						<div class="item-data item-download">

							<div class="down-title">
								<h4 class="title-g__fz--25 fontXBold colorBlue"><?php echo get_the_title(); ?></h4>
							</div>

							<div class="down-size">
								<h4><?php if (get_field('upload_file_doc')): echo $filesize; endif;  ?></h4>
							</div>

							<div class="down-link">
								<div class="title-g__buttons">
				                    <div class="title-g__button">
				                        <a class="button-g button-g--green g--uppercase" target="_blank" href="<?php echo $url; ?>">
				                            <span class="button-g__text">Descargar</span>
				                        </a>
				                    </div>
				                </div>

							</div>

						</div>

					</div>		
								    
				    
				   <?php endwhile;

				    wp_reset_postdata(); 
				  ?>	

		

				<!-- ends loop -->

			</div>

	        
	        <div class="block-g__listPagination">
	            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
	        </div>

		</div>



	</div>

</section>


<?php get_template_part( 'templates/blog/content', 'blog'); ?>
<?php get_footer(); ?>