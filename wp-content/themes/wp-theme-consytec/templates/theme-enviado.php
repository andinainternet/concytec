<?php
/* Template Name: Template - Enviado */
get_header(); ?>

<?php 
if(have_posts()) : while(have_posts()) : the_post(); ?>
<div class="message-g message-g--sent message-g--default">
	<div class="message-g__container wrapper-container">
		<div class="message-g__image">
			<img src="<?php the_post_thumbnail_url(); ?>" alt="">
		</div>
		<div class="message-g__title">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="message-g__text">
			<?php the_content(); ?>
		</div>
		<div class="message-g__button">
			<a class="button-g button-g--green g--uppercase" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<span class="button-g__text"><?php _e('Volver al inicio','concytec'); ?></span>
			</a>
		</div>
	</div>
</div>
<?php 
endwhile; 
wp_reset_query();
endif;
?>

<?php get_footer(); ?>