<?php
/* Template Name: Template - Grid Post */
get_header(); 


$headerBanner = get_field( 'header_banner_single_page' );

/**checkbox**/
$headTitle = get_field( 'single_title' );

$cardReadMore = get_field( 'texto_ver_mas' );

?>




<section class="header-single">
	<div class="bg-single bg-single-video" style="background-image: url(<?php echo get_field( 'header_banner_single_page' ); ?>)">
			<h1 class="titleSingle title-g__fz--55 fontXBold title-g__block"><?php echo get_the_title(); ?></h1>
	</div>
</section>


<section class="wrapper-padding">
	
	<div class="wrapper-container stylesAll-content">
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up" style="border: none;">
            	<?php if ($headTitle): ?>
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                    	<!-- optional -->
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block"><?php echo $headTitle; ?></strong>
                    </h3>
                </div>
            	<?php endif ?>
            </div>
		<br>

		<div class="wrapper-grid">
			<div class="consy-row">
			
				<!-- start loop -->
			<?php 
				/**
				 * Setup query to show the ‘services’ post type with ‘8’ posts.
				 * Output the title with an excerpt.
				 */
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

					$value     = get_post_meta( $post->ID, '_wporg_meta_key', true );
					$value_cat = get_post_meta( $post->ID, 'wporg_field_cat', true );

					$value_tax = get_post_meta( $post->ID, 'wporg_field_taxonomy', true );

					if ($value_tax == 'null') {
						# code...
					    $args = array(  
					        'post_type' => $value,
					        'post_status' => 'publish',
					        // 'taxonomy' => $value_tax,
					        'posts_per_page' => 10, 
					        'orderby' => 'date', 
					        'order' => 'ASC', 
					        'paged' => $paged
					    );							
					} else {
						$args = array(  
					        'post_type' => $value,
					        'post_status' => 'publish',
					        // 'taxonomy' => $value_tax,

							'tax_query' =>  array(
	    									'relation' => 'AND',array(
									            'taxonomy' => $value_cat, // Taxonomy, in my case I need default post categories
									            'field'    => 'slug',
									            'terms'    => $value_tax, // Your category slug (I have a category 'interior')
								        		)
											),

					        'posts_per_page' => 10, 
					        'orderby' => 'date', 
					        'order' => 'ASC', 
					        'paged' => $paged
					    );					    					
					}


				    $wp_query = new WP_Query( $args ); 
				        
				    while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<div class="consy-col consy-col-lg-6 consy-col-12">
						<div class="item-img">
							<figure>
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							</figure>
						</div>

						<div class="item-data">
							<div class="data--date">
								<h5><?php echo get_the_date(); ?></h5>
							</div>
							<div class="data-title">
								<h4 class="title-g__fz--20 fontXBold colorBlue"><?php echo get_the_title() ?></h4>
							</div>
							<div class="data--link">
								<h5>
									<a class="more-post titleText-g__fz--18 fontXBold colorGreen" href="<?php echo get_the_permalink() ?>">
										<?php if ($cardReadMore): ?>
											<span><?php echo $cardReadMore; ?></span>	
										<?php else: ?>
											<span><?php echo 'Ver más'; ?></span>	
										<?php endif ?>
										
									</a>
								</h5>
							</div>

						</div>
					</div>		
								    
				    
				   <?php endwhile;

				    wp_reset_postdata(); 
				  ?>	

		

				<!-- ends loop -->

			</div>

	        
	        <div class="block-g__listPagination">
	            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
	        </div>

		</div>



	</div>

</section>


<?php get_template_part( 'templates/blog/content', 'blog'); ?>
<?php get_footer(); ?>