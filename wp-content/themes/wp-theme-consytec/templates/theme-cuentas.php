<?php
/* Template Name: Template - Rendición de cuentas */
get_header(); ?>


<!-- Componente 1 del home -->



<section class="block-g component-block--banner bg-color-azul" id="componente1">
			    
			    <div class="block-g__container wrapper-container">  
			        <div class="title-g block-g__title">
			            <div class="title-g__title">
			    

			                <?php
			                    $blockcomp1_title = get_sub_field('titulo_componente1');
			                    // $blockcomp1_title = 'Encuentros y seminarios';

			                    $blockcomp1_title = get_sub_field('titulo_componente1');
								// $blockcomp1_subtitle = 'Espacios de socialización, conocimiento y sinergias';

			                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
			                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
			                    $blockcomp1_array2 = $blockcomp1_titleArray[1];
			                    ?>
			                    <h2 class="g--uppercase">
			                        <strong class="title-g__fz--70 fontXBold colorGreen title-g__block" style="line-height: 1.3; margin-bottom: 6%;"><?php echo get_field( 'titulo_rendicion' );;?></strong>
			                        <span class="title-g__fz--48 fontBold colorWhite title-g__block" style="line-height: 1.5; font-family: 'Montserrat', sans-serif !important; font-weight: 700; "><?php echo get_field( 'subtitulo_rendicion' ); ?></span>
			                    </h2>
			                    
			            </div>

			                <?php wp_reset_postdata(); ?>
			        </div>
			        <div class="block-g__image">
			            <div class="block-g__figure figure_bg_video"></div>
			            <?php
			                // $image_relational = get_sub_field('componente_image'); 
			                $image_relational= get_field( 'imagen_rendicion' );;
			                ?>
			                <figure class="block-g__img">
			                    <!-- <?php //the_post_thumbnail() ?> -->
			                    <img src="<?php echo $image_relational; ?>" alt="">
			                </figure>
			                <?php wp_reset_postdata(); ?>
			        </div>
			    </div>
			</section>

<!-- <?php //elseif( get_row_layout() == 'equipos' ):  ?> -->

<section class="block-g equipamientos-block--equipos bg-color-celeste">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title title-rendicion">
            <div class="title-g__title" >
                <?php
                $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina6');
                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--64 fontBold colorWhite"><?php echo $blockcomp1_array1?></span>
                    <strong class="title-g__fz--80 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>
                </h2>
            </div>
                <div class="title-g__text colorWhite fontRegular titleText-g__fz--27" >
                    <?php 
                    	echo get_field( 'contenido_rendicion' );;

                    	?>
                </div>
        </div>
        <?php //if(get_sub_field('imagen_equipamiento_pagina6')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img" >
                <!-- <img src="<?php //the_sub_field('imagen_equipamiento_pagina6') ?>" alt=""> -->
                <img src="<?php echo get_field( 'imagen_rendicion_2' ); ?>" alt="">
            </figure>
        </div>
        <?php //endif; ?>
    </div>
</section> 

<!-- loop custom post type -->

<section class="wrapper-padding">
	
	<div class="wrapper-container stylesAll-content">
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up" style="text-align: center; border: none;">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--45 fontXBold colorBlue title-g__block">DOCUMENTOS OFICIALES</strong>
                        
                    </h3>
                </div>
            </div>
		<br>

		<?php 

		$feature_post = get_field( 'feature_post_rend' );

		// echo '<pre>';
		// var_dump($feature_post);
		// echo '</pre>';

		 ?>

		<div class="wrapper-grid">
			<div class="consy-row">
			
				<!-- start loop -->
			<?php 
				/**
				 * Setup query to show the ‘services’ post type with ‘8’ posts.
				 * Output the title with an excerpt.
				
				 */

 				$posttypes= array_values(get_post_types()); // get all post types
  				$exclude_post_type=array('post'); // exclude the post types which you don't want to include
  				$final_posttypes = array_diff($posttypes, $exclude_post_type); // this is final array which will use in post_type.

					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				    $args = array(  
				        'post_type' => $final_posttypes,
				        'post__in' =>  $feature_post,
				        'post_status' => 'publish',
				        'posts_per_page' => 10, 
				        'orderby' => 'date', 
				        'order' => 'ASC', 
				        'paged' => $paged
				    );

				    $wp_query = new WP_Query( $args ); 
				        
				    while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

				    	<?php 
				    		$singular_name = get_post_type_object(get_post_type(get_the_ID()))->labels->singular_name


				    	 ?>


					<div class="consy-col consy-col-lg-6 consy-col-12">
						<div class="item-img">
							<figure>
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							</figure>
						</div>

						<div class="item-data">
							<div class="data--date">
								<h5><?php echo get_the_date(); ?></h5>
							</div>
							<div class="data-title">
								<h4 class="title-g__fz--20 fontXBold colorBlue"><?php echo get_the_title() ?></h4>
							</div>
							<div class="data--link">
								<h5><a class="more-post titleText-g__fz--18 fontXBold colorGreen" href="<?php echo get_the_permalink() ?>">Ver <?php echo strtolower($singular_name); ?> completo</a></h5>
							</div>

						</div>
					</div>		
								    
				    
				   <?php endwhile;

				    wp_reset_postdata(); 
				  ?>	

		

				<!-- ends loop -->

			</div>

	        
	        <div class="block-g__listPagination">
	            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
	        </div>

		</div>



	</div>

</section>


<?php get_template_part( 'templates/blog/content', 'blog'); ?>
<?php get_footer(); ?>