<a class="blog-g__item" href="<?php the_permalink() ?>">
    <figure class="blog-g__itemImage">
        <?php the_post_thumbnail() ?>
    </figure>
    <div class="blog-g__itemInfo">
        <div class="blog-g__itemInfoTitle">
            <h3><?php the_title() ?></h3>
        </div>
        <div class="blog-g__itemInfoDate">
            <span><?php echo time_ago(); ?></span>
        </div>
    </div>
</a>