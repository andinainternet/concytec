<section class="block-g__blog">
    <div class="block-g__blogContainer wrapper-container">
        <?php if(get_field('titulo_noticias','options')) :?>
            <!-- <div class="title-g block-g__blogTitle" data-aos="fade-up"> -->
            <div class="title-g block-g__blogTitle">                
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                    <?php
                        $noticies_title = get_field('titulo_noticias','options');
                        $noticies_titleArray = preg_split( "/[|]/", $noticies_title );
                        $notice_array1 = $noticies_titleArray[0];
                        $notice_array2 = $noticies_titleArray[1];
                    ?>	
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block"><?php echo $notice_array1; ?></strong>
                        <span class="title-g__fz--30 fontLight colorSilver title-g__block"><?php echo $notice_array2; ?></span>
                    </h3>
                </div>
            </div>
        <?php endif ?>
        <!-- <div class="block-g__blogPosts" data-aos="fade-up"> -->
        <div class="block-g__blogPosts">
            <?php
            $post_objects = get_field('noticias_destacadas','options');
            if( $post_objects ): ?>
                <div class="block-g__blogPostDesktop">
                    <?php foreach( $post_objects as $post):  ?>
                        <?php setup_postdata($post); ?>
                        <article class="block-g__blogPost">
                            <?php get_template_part( 'templates/blog/content', 'post'); ?>
                        </article>
                    <?php endforeach; ?>
                </div>
                <div class="block-g__blogPostMobile block-g--blogPostMobile list--none">
                    <ul class="swiper-wrapper">
                    <?php foreach( $post_objects as $post):  ?>
                        <?php setup_postdata($post); ?>
                        <li class="swiper-slide">
                            <article class="block-g__blogPost">
                                <?php get_template_part( 'templates/blog/content', 'post'); ?>
                            </article>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    <span class="block-g--blogPostMobilePrev icon-flechaderecha"></span>
                    <span class="block-g--blogPostMobileNext icon-flechaderecha"></span>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
        <?php if(get_field('boton_url_noticias','options')) :?>
            <div class="block-g__blogButton" data-aos="fade-up">
                <a class="button-g button-g--blue g--uppercase" href="<?php the_field('boton_url_noticias','options')?>">
                    <span class="button-g__text"><?php the_field('boton_label_noticias','options')?></span>
                </a>
            </div>
        <?php endif ?>
    </div>
</section>