<?php
/* Template Name: Template - Contacto */
get_header(); ?>

<!--Contact-->
<section class="contact-block">
    <div class="contact-block__container wrapper-container">
        <span class="contact-block__figure"></span>
        <div class="title-g contact-block__title">
            <div class="title-g__title">
                <h1 class="g--uppercase title-g__fz--48 fontXBold colorBlue"><?php the_field('titulo1_contacto')?></h1>
            </div>
        </div>
        <div class="contact-block__mapForm">

            <div class="contact-block__infoMap">
                <div class="contact-block__info">
                    <div class="contact-block__infoTitles">
                        <h2><?php the_field('titulo2_contacto') ?></h2>
                        <h3><?php the_field('titulo3_contacto') ?></h3>
                    </div>
                    

                    <!-- check if relclamo or contacto -->

                    <?php 

                        $form = get_field( 'tipo_de_formulario' );
                        // var_dump($form);
                     ?>

                    <?php if ($form == 'reclamo' ): ?>
                           
                </div>
                            
                        <?php if( get_field('texto_formulario_reclamo') ): ?>
                        <div class="title-g__text colorBlue fontRegular titleText-g__fz--18">
                            <?php the_field('texto_formulario_reclamo') ?>
                        </div>
                        <?php endif; ?>

                    <?php elseif ($form == 'contacto' ): ?>
                        
                        <?php if(get_field('direccion','options') ):  ?>
                            
                            <div class="contact-block__address">
                                <?php the_field('direccion','options') ?>
                            </div>
                            
                        <?php endif; ?>
                            
                        <?php if(get_field('correo','options') ):  ?>
                            
                            <div class="contact-block__mail">
                                <a href="mailto:<?php the_field('correo','options') ?>"><?php the_field('correo','options') ?></a>
                            </div>
                            
                        <?php endif; ?>
                            
                        <?php if( have_rows('central_telefonica','options') ): ?>
                            
                            <?php while( have_rows('central_telefonica','options') ): the_row(); ?>
                            
                                <div class="contact-block__central">
                                    <h3><?php the_sub_field('titulo_central_telefonica','options') ?></h3>
                                    <a href="tel:<?php the_sub_field('numero_central_telefonica','options') ?>"><?php the_sub_field('numero_central_telefonica','options') ?></a>
                                </div>
                            <?php endwhile; ?>
                            
                        <?php endif; ?>
                            


                            <div class="contact-block__social list--none">
                            
                            <?php if( have_rows('lista_redes_sociales', 'options') ): ?>
                                <ul class="main-footer__socialMedia">
                                    <?php while( have_rows('lista_redes_sociales', 'options') ): the_row(); ?>
                                        <?php 
                                            $name_select_sub_field = (get_sub_field_object('red_social_lista_redes_sociales'));
                                            $name_sub_field = get_sub_field('red_social_lista_redes_sociales');
                                            $label_select = ($name_select_sub_field['choices'][$name_sub_field]);
                                        ?>
                                        
                                        <?php if( get_sub_field('url_lista_redes_sociales', 'options') ) { ?>
                                        <li>
                                        <a class="icon-<?php the_sub_field( 'red_social_lista_redes_sociales' ); ?>" href="<?php the_sub_field('url_lista_redes_sociales', 'options'); ?>" target="_blank"> </a>
                                        </li>
                                        <?php } ?>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?> 
                            </div>
                        </div>
                        <?php if( get_field('google_maps_iframe') ): ?>
                        <div class="contact-block__map">
                            <?php the_field('google_maps_iframe') ?>
                        </div>
                        <?php endif; ?>

                    <?php endif ?>

                        

                <!-- end check if contact or reclam -->



            </div>
            <div class="contact-block__form">
                <span class="contact-block__formFigure"></span>
                <div class="title-g contact-block__formTitle">
                    <div class="title-g__title">
                        <h3 class="title-g__fz--21 fontXBold colorBlue"><?php the_field('titulo_formulario')?></h3>
                    </div>
                    <div class="title-g__text colorBlue fontRegular titleText-g__fz--16">
                        <?php the_field('texto_formulario')?>
                    </div>
                </div>
                <div class="contact-block__formForm">
                <?php
                    $post_object = get_field('formulario_formulario');
                    if( $post_object ): 
                        $post = $post_object;
                        setup_postdata( $post ); 
                                $cf7_id = $p->ID;
                                echo do_shortcode( '[contact-form-7 id="'.$cf7_id.'" ]' ); 
                    wp_reset_postdata();
                    endif;
                ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>


<?php get_footer(); ?>