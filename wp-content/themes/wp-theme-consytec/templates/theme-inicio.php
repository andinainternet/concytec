<?php
/* Template Name: Template - Inicio*/
get_header(); ?>

<!-- HOME - CONTENIDO FLEXIBLE -->  
<?php if( have_rows('contenido_inicio') ): ?>
<?php while( have_rows('contenido_inicio') ): the_row(); ?>
<?php if( get_row_layout() == 'baner_principal' ): ?>

<!--Main Slider-->     
<?php if( have_rows('lista_banerprincipal') ): ?>
<section class="slider-principal slider-principal--js list--none">
    <ul class="slider-principal__list swiper-wrapper">
        <?php while( have_rows('lista_banerprincipal') ): the_row(); ?>
        
            
                <a target="_blank" href="<?php echo get_sub_field( 'boton_url_banerprincipal' ); ?>" class="swiper-slide slider-desktop" style="background-image: url( <?php echo get_sub_field('imagen_banerprincipal'); ?> );"></a>

                <a target="_blank" href="<?php echo get_sub_field( 'boton_url_banerprincipal' ); ?>" class="swiper-slide slider-mobile" style="background-image: url(<?php echo get_sub_field('imagenm_banerprincipal'); ?>);"></a>

        <?php endwhile; ?>
    </ul>
    <div class="slider--principalPagination"></div>
    <div class="slider-principalIcon icon-scroll-up ancla--js--top"></div>
</section>
<?php endif; ?>


<?php elseif( get_row_layout() == 'proyectos' ):  ?>    

<!--Projects-->
<section class="home-projects" style="background-image: url(<?php the_sub_field('imagen_inicio_b1') ?>)">
    <div class="home-projects__container wrapper-container">
        <figure class="home-projects__image" style="background-image: url(<?php the_sub_field('imagen_inicio_b1') ?>)"></figure>
        <div class="home-projects__info">
            <div class="title-g home-projects__title">
                <?php if(get_sub_field('titulo_inicio_b1')) : ?>
                <div class="title-g__title">
                    <?php   
                        $homeb1_title = get_sub_field('titulo_inicio_b1');
                        $homeb1_titleArray = preg_split( "/[|]/", $homeb1_title );
                        $homeb1_array1 = $homeb1_titleArray[0];
                        $homeb1_array2 = $homeb1_titleArray[1];
                    ?>
                    <h2 class="g--uppercase">
                        <span class="title-g__fz--46 fontXBold colorBlue2 title-g__block"><?php echo $homeb1_array1 ?></span>
                        <strong class="title-g__fz--104 fontXBold colorGreen title-g__block"><?php echo $homeb1_array2 ?></strong>
                    </h2>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('texto_inicio_b1')) : ?>
                <div class="title-g__text colorBlack fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_inicio_b1') ?>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('botonurl_inicio_b1')) : ?>
                <div class="title-g__buttons">
                    <div class="title-g__button">
                        <a class="button-g button-g--lightBlue g--uppercase" href="<?php the_sub_field('botonurl_inicio_b1') ?>">
                            <span class="button-g__text"><?php the_sub_field('botonlabel_inicio_b1') ?></span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'resultados' ):  ?>

<!--Results-->
<section class="home-results" style="background-image: url(<?php the_sub_field('imagen_inicio_b2') ?>)">
    <figure class="home-results__image" style="background-image: url(<?php the_sub_field('imagen_inicio_b2') ?>)"></figure>
    <div class="home-results__container wrapper-container">
        <div class="title-g home-results__title">
            <?php if(get_sub_field('titulo_inicio_b2')) : ?>
            <div class="title-g__title">
                <?php   
                    $homeb2_title = get_sub_field('titulo_inicio_b2');
                    $homeb2_titleArray = preg_split( "/[|]/", $homeb2_title );
                    $homeb2_array1 = $homeb2_titleArray[0];
                    $homeb2_array2 = $homeb2_titleArray[1];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--46 fontXBold colorWhite title-g__block"><?php echo $homeb2_array1 ?></span>
                    <strong class="title-g__fz--104 fontXBold colorLightBlue2 title-g__block"><?php echo $homeb2_array2 ?></strong>
                </h2>
            </div>
            <?php endif; ?>
        </div>
        <div class="home-results__infoListButton">
            <div class="home-results__info">
                <?php if( have_rows('lista_inicio_b2') ): ?>
                <div class="home-results__list">
                    <?php while( have_rows('lista_inicio_b2') ): the_row(); ?>
                    <article class="home-results__listItem">
                        <div class="home-results__listItemFigure">
                            <span><?php the_sub_field('numero_lista_inicio_b2') ?></span>
                        </div>
                        <div class="home-results__listItemText">
                            <h3><?php the_sub_field('texto_lista_inicio_b2') ?></h3>
                        </div>
                    </article>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php if( get_sub_field('botonurl_inicio_b2') ): ?>
            <div class="home-results__button">
                <a class="button-g button-g--lightBlue g--uppercase" target="_blank" href="<?php the_sub_field('botonurl_inicio_b2') ?>">
                    <span class="button-g__text"><?php the_sub_field('botonlabel_inicio_b2') ?></span>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'componentes' ):  ?>

<!--Components-->
<section class="home-components">
    <div class="home-components__figure"></div>
    <div class="home-components__container wrapper-container">
        <div class="title-g home-components__title">
            <div class="title-g__title">
                <h2 class="g--uppercase title-g__fz--104 fontXBold colorBlue">
                    <?php the_sub_field('titulo_inicio_b2') ?>
                </h2>
            </div> 
        </div>
        <?php if( have_rows('lista_inicio_b3') ): ?>
            <div class="home-components__items ancla-g">
            <?php $x =1; ?>
            <?php while( have_rows('lista_inicio_b3') ): the_row(); ?>
            <div class="home-components__item" data-ancla="componente<?php echo $x;?>" data-offset="header">
                <a href="#componente<?php echo $x;?>">
            <?php
            $componentSubtitle = get_sub_field('subtitulo_anadido_inicio_b3');
            $componenttitle = get_sub_field('titulo_anadido_inicio_b3');
            $componenttitle2 = get_sub_field('titulo2_anadido_inicio_b3');
            $post_object = get_sub_field('componente_inicio_b3');
            if( $post_object ): 
                $post = $post_object;
                setup_postdata( $post ); ?>
                    <div class="home-components__itemNumber" >
                        <?php echo $x; ?>
                    </div>
                    <div class="home-components__itemInfo">
                        <span class="g--uppercase"><?php echo $componentSubtitle; ?></span>
                        <h3 class="g--uppercase"><?php echo $componenttitle; ?></h3>
                        <h4><?php echo $componenttitle2; ?></h4>
                    </div>
                                
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            </a>
            </div>
            <?php $x++; ?>
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>


<?php elseif( get_row_layout() == 'componente1' ):  ?>
    
    <section class="block-g block-g--component1" id="componente1">
        <div class="block-g__container wrapper-container">  
            <div class="title-g block-g__title">
                <div class="title-g__title">
                    <?php if(get_sub_field('titulo_componente1') ):  ?>
                    <?php
                        $blockcomp1_title = get_sub_field('titulo_componente1');
                        $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                        $blockcomp1_array1 = $blockcomp1_titleArray[0];
                        $blockcomp1_array2 = $blockcomp1_titleArray[1];
                        ?>
                        <h2 class="g--uppercase">
                            <strong class="title-g__fz--104 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array1?></strong>
                            <span class="title-g__fz--46 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></span>
                        </h2>
                    <?php endif; ?>
                    <?php if(get_sub_field('subtitulo_componente1') ):  ?>
                        <h3>
                            <span class="title-g__fz--32 fontXBold colorWhite title-g__block g--uppercase"><?php the_sub_field('subtitulo_componente1')?></span>
                        </h3>
                    <?php endif; ?>
                </div>

                <?php
                    $post_object = get_sub_field('componente_componente1');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                        <?php the_content(); ?>
                    </div>
                    <div class="title-g__buttons">
                        <div class="title-g__button">
                            <a class="button-g button-g--lightBlue2 g--uppercase" href="<?php the_permalink(); ?>">
                                <span class="button-g__text"><?php _e('Mas información','concytec')?></span>
                            </a>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="block-g__image">
                <div class="block-g__figure"></div>
                <?php
                    $post_object = get_sub_field('componente_componente1');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <figure class="block-g__img">
                        <?php the_post_thumbnail() ?>
                    </figure>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <?php elseif( get_row_layout() == 'componente2' ):  ?>
    <section class="block-g block-g--component2" id="componente2">
        <div class="block-g__container wrapper-container">  
            <div class="title-g block-g__title">
                <div class="title-g__title">
                    <?php if(get_sub_field('titulo_componente2') ):  ?>
                    <?php
                        $blockcomp1_title = get_sub_field('titulo_componente2');
                        $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                        $blockcomp1_array1 = $blockcomp1_titleArray[0];
                        $blockcomp1_array2 = $blockcomp1_titleArray[1];
                        $blockcomp1_array3 = $blockcomp1_titleArray[2];
                        ?>
                        <h2 class="g--uppercase">
                            <span class="title-g__fz--46 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array1?></span>
                            <strong class="title-g__fz--104 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>
                            <i class="title-g__fz--104 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array3?></i>
                        </h2>
                    <?php endif; ?>
                    <?php if(get_sub_field('subtitulo_componente2') ):  ?>
                        <h3>
                            <span class="title-g__fz--32 fontXBold colorWhite title-g__block g--uppercase"><?php the_sub_field('subtitulo_componente2')?></span>
                        </h3>
                    <?php endif; ?>
                </div>

                <?php
                    $post_object = get_sub_field('componente_componente2');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                        <?php the_content(); ?>
                    </div>
                    <div class="title-g__buttons">
                        <div class="title-g__button">
                            <a class="button-g button-g--lightBlue g--uppercase" href="<?php the_permalink(); ?>">
                                <span class="button-g__text"><?php _e('Mas información','concytec')?></span>
                            </a>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="block-g__image">
                <div class="block-g__figure"></div>
                <?php
                    $post_object = get_sub_field('componente_componente2');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <figure class="block-g__img">
                        <?php the_post_thumbnail() ?>
                    </figure>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <?php elseif( get_row_layout() == 'componente3' ):  ?>
    <section class="block-g block-g--component3" id="componente3">
        <div class="block-g__container wrapper-container">  
            <div class="title-g block-g__title">
                <div class="title-g__title">
                    <?php if(get_sub_field('titulo_componente3') ):  ?>
                    <?php
                        $blockcomp1_title = get_sub_field('titulo_componente3');
                        $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                        $blockcomp1_array1 = $blockcomp1_titleArray[0];
                        $blockcomp1_array2 = $blockcomp1_titleArray[1];
                        ?>
                        <h2 class="g--uppercase">
                            <span class="title-g__fz--46 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array1?></span>
                            <strong class="title-g__fz--104 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>

                        </h2>
                    <?php endif; ?>
                    <?php if(get_sub_field('subtitulo_componente3') ):  ?>
                        <h3>
                            <span class="title-g__fz--32 fontXBold colorWhite title-g__block g--uppercase"><?php the_sub_field('subtitulo_componente3')?></span>
                        </h3>
                    <?php endif; ?>
                </div>

                <?php
                    $post_object = get_sub_field('componente_componente3');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                        <?php the_content(); ?>
                    </div>
                    <div class="title-g__buttons">
                        <div class="title-g__button">
                            <a class="button-g button-g--lightBlue g--uppercase" href="<?php the_permalink(); ?>">
                                <span class="button-g__text"><?php _e('Mas información','concytec')?></span>
                            </a>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="block-g__image">
                <div class="block-g__figure"></div>
                <?php
                    $post_object = get_sub_field('componente_componente3');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <figure class="block-g__img">
                        <?php the_post_thumbnail() ?>
                    </figure>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <?php elseif( get_row_layout() == 'componente4' ):  ?>
    <section class="block-g block-g--component4" id="componente4">
        <div class="block-g__container wrapper-container">  
            <div class="title-g block-g__title">
                <div class="title-g__title">
                    <?php if(get_sub_field('titulo_componente4') ):  ?>
                    <?php
                        $blockcomp1_title = get_sub_field('titulo_componente4');
                        $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                        $blockcomp1_array1 = $blockcomp1_titleArray[0];
                        $blockcomp1_array2 = $blockcomp1_titleArray[1];
                        ?>
                        <h2 class="g--uppercase">
                            <strong class="title-g__fz--104 fontXBold colorBlue title-g__block"><?php echo $blockcomp1_array1?></strong>
                            <span class="title-g__fz--46 fontXBold colorBlue title-g__block"><?php echo $blockcomp1_array2?></span>
                        </h2>
                    <?php endif; ?>
                    <?php if(get_sub_field('subtitulo_componente4') ):  ?>
                        <h3>
                            <span class="title-g__fz--32 fontXBold colorGreen title-g__block g--uppercase"><?php the_sub_field('subtitulo_componente4')?></span>
                        </h3>
                    <?php endif; ?>
                </div>

                <?php
                    $post_object = get_sub_field('componente_componente4');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                        <?php the_content(); ?>
                    </div>
                    <div class="title-g__buttons">
                        <div class="title-g__button">
                            <a class="button-g button-g--lightBlue g--uppercase" href="<?php the_permalink(); ?>">
                                <span class="button-g__text"><?php _e('Mas información','concytec')?></span>
                            </a>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="block-g__image">
                <div class="block-g__figure"></div>
                <?php
                    $post_object = get_sub_field('componente_componente4');
                    if( $post_object ): 
                    $post = $post_object;
                    setup_postdata( $post ); ?>
                    <figure class="block-g__img">
                        <?php the_post_thumbnail() ?>
                    </figure>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>



<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>