

<div class="g-item g-item__inv">
    <figure class="g-item__img programa-item" style="background-image: url(<?php the_post_thumbnail_url() ?>);">
        
    </figure>
    <div class="g-item__info">
        <div class="g-item__infoTitle">
            <h3><?php echo string_limit(get_the_title(),  50, ' ...') ?></h3>
        </div>
        <div class="g-item__infoContent">
            <?php the_excerpt_max_charlength(300) ?>
        </div>
        <div class="g-item__infoButton">
            <a class="button-g button-g--green g--uppercase" href="<?php the_permalink() ?>">
                <span class="button-g__text"><?php _e('Ver Programa','concytec') ?></span>
            </a>
        </div>
    </div>
</div>