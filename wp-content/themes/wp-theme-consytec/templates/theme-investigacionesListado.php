<?php
/* Template Name: Template - Investigaciones Listado*/
get_header(); ?>

<!-- Investigaciones Banner-->
<?php get_template_part( 'templates/investigaciones/content', 'banner'); ?>

<!-- Investigaciones Listado-->
<section class="block-g__list">
    <div class="block-g__listContainer wrapper-container">
        <div class="block-g__listNav">
            <?php get_template_part( 'templates/investigaciones/content', 'menu'); ?>
        </div>
        <div class="block-g__listTitle g--uppercase">
            <h2><?php the_title() ?></h2>
        </div>
        <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
        <?php
        $args = array( 
        'post_type' => 'investigacion', 
        'post_status' => 'publish',
        'posts_per_page' => '12',
        'paged' => $paged,
        );
        $wp_query = new WP_Query($args); ?>
        <?php if($wp_query->have_posts()) : ?>
        <div class="block-g__listItems">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <article class="block-g__listItem">
                    <?php get_template_part( 'templates/investigaciones/content', 'investigacion'); ?>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        <div class="block-g__listPagination">
            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
        </div>
        <?php endif; ?>
    </div>   
</section>

<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>