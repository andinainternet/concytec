<?php
/* Template Name: Template - Noticias */
get_header(); ?>


<section class="header-single">
	<div class="bg-single" style="background-image: url(<?php echo get_field( 'header_banner_single_page' ); ?>)">
		<h1 class="titleSingle title-g__fz--55 fontXBold title-g__block"><?php echo get_the_title(); ?></h1>
	</div>
</section>

<section class="wrapper-padding">
	
	<div class="wrapper-container stylesAll-content">
		<div class="title-g block-g__blogTitle aos-init aos-animate" data-aos="fade-up" style="border: none;">
                <div class="title-g__title">
                    <h3 class="g--uppercase">
                        <strong class="title-g__fz--30 fontXBold colorBlue title-g__block">Noticias recientes</strong>
                        
                    </h3>
                </div>
            </div>
		<br>

		<div class="wrapper-grid">
			<div class="consy-row">
			
				<!-- start loop -->
			<?php 
				/**
				 * Setup query to show the ‘services’ post type with ‘8’ posts.
				 * Output the title with an excerpt.
				 */
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				    $args = array(  
				        'post_type' => 'noticia',
				        'post_status' => 'publish',
				        'posts_per_page' => 10, 
				        'orderby' => 'date', 
				        'order' => 'DESC', 
				        'paged' => $paged
				    );

				    $wp_query = new WP_Query( $args ); 
				        
				    while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<div class="consy-col consy-col-lg-6 consy-col-12">
						<div class="item-img">
							<figure>
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							</figure>
						</div>

						<div class="item-data">
							<div class="data--date">
								<h5><?php echo get_the_date(); ?></h5>
							</div>
							<div class="data-title">
								<h4 class="title-g__fz--20 fontXBold colorBlue"><?php echo get_the_title() ?></h4>
							</div>
							<div class="data--link">
								<h5><a class="more-post titleText-g__fz--18 fontXBold colorGreen" href="<?php echo get_the_permalink() ?>">Leer noticia completa</a></h5>
							</div>

						</div>
					</div>		
								    
				    
				   <?php endwhile;

				    wp_reset_postdata(); 
				  ?>	

		

				<!-- ends loop -->

			</div>

	        
	        <div class="block-g__listPagination">
	            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
	        </div>

		</div>



	</div>

</section>


<?php get_template_part( 'templates/blog/content', 'blog'); ?>
<?php get_footer(); ?>