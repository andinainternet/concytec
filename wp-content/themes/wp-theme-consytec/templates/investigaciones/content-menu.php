
<?php  
$taxonomy     = 'categoria-investigacion';
$taxonomy_2     = 'sectores';
$orderby      = 'ASC'; 
$show_count   = false;
$pad_counts   = false;
$hierarchical = true;
$title        = '';

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title
);

$args_2 = array(
    'taxonomy'     => $taxonomy_2,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title
);
    // echo '<pre>';
    // var_dump(get_categories( $args ) );
    // echo '</pre>';

    $url = $_SERVER['REQUEST_URI'];
    
    $end = array_slice(explode('/', rtrim($url, '/')), -1)[0];
    
    // var_dump($end);

?>
<div class="g-menu__categories list--none content-categories">
    <select name="research-categories" id="researchCategories">

    <?php 





        foreach (get_categories( $args ) as $key) {

            if ($key->slug == $end) {
                echo '<option selected value="' . get_category_link( $key->term_id ) . '">'  . $key->name . '</option>';
            } else {
                echo '<option value="' . get_category_link( $key->term_id ) . '">'  . $key->name . '</option>';

            }
        }

        foreach (get_categories( $args_2 ) as $key_2) {
            if ($key_2->slug == $end) {
                echo '<option selected value="' . get_category_link( $key_2->term_id ) . '">'  . $key_2->name . '</option>';
            } else {
                echo '<option value="' . get_category_link( $key_2->term_id ) . '">'  . $key_2->name . '</option>';

            }
        }    

    ?>
    </select>

 </div>

<script>
    jQuery(document).ready(function($) {
        
        jQuery(function(){
          // bind change event to select
          jQuery('#researchCategories').on('change', function () {
              var url = $(this).val(); // get selected value
              if (url) { // require a URL
                  window.location = url; // redirect
              }
              return false;
          });
        });
    });
</script>

<style>
    
    #researchCategories {
        padding: 10px 30px;
        font-size: 18px;
        border-radius: 15px;
        /*margin-left: 2.5%;*/
    }

    .content-categories {
        text-align: center;
    }    


@media only screen and (min-width: 767px) {
    #researchCategories {
        padding: 10px 30px;
        font-size: 18px;
        border-radius: 15px;
        /*margin-left: 2.5%;*/
    }
}

@media only screen and (max-width: 767px) {
    #researchCategories {
        padding: 5px 10px;
        font-size: 15px;
        border-radius: 15px;
        margin-left: 1%;
    }

    .content-categories {
        text-align: center;
    }    
}

</style>
