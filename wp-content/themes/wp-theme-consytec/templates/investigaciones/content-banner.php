<?php if( have_rows('carrousel_bloqueg', 395) ): ?>
<?php while( have_rows('carrousel_bloqueg', 395) ): the_row(); ?>
<section class="block-g inv-list--banner">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title">
        <?php if(get_sub_field('titulo_carrousel_bloqueg', 395) ):  ?>
            <div class="title-g__title">
                <?php
                $blockcomp1_title = get_sub_field('titulo_carrousel_bloqueg',395);
                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--64 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array1?></span>
                    <strong class="title-g__fz--80 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>
                </h2>
            </div>
            <?php if(get_sub_field('texto_carrousel_bloqueg',395) ):  ?>
                <div class="title-g__text colorWhite title-g__fz--35 fontRegular">
                    <?php the_sub_field('texto_carrousel_bloqueg',395) ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_carrousel_bloqueg',395)) :?>
        <div class="block-g__image">
            <div class="block-g__figure equipos-banner"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_carrousel_bloqueg',395) ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section> 
<?php endwhile; ?>
<?php endif; ?>