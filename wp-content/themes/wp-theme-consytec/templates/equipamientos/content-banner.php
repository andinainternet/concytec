<section class="equipamiento-g__slide equipamiento-g--slide slider-principal--js list--none">
    <ul class="swiper-wrapper">
            <?php if( have_rows('carrousel_bloqueg', 194) ): ?>
            <?php while( have_rows('carrousel_bloqueg', 194) ): the_row(); ?>
            <li class="swiper-slide">
            <?php if( get_sub_field('tipo_carrousel_bloqueg', 194) == 'default' ) { ?>
                <section class="block-g inv-list--banner inv-list--banner1">
                    <div class="block-g__container wrapper-container">  
                        <div class="title-g block-g__title">
                            <?php if(get_sub_field('titulo_carrousel_bloqueg', 194) ):  ?>
                                <div class="title-g__title">
                                    <?php
                                    $blockcomp1_title = get_sub_field('titulo_carrousel_bloqueg',194);
                                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
                                    $blockcomp1_array2 = $blockcomp1_titleArray[1];
                                    ?> 
                                    <h2 class="g--uppercase">
                                        <span class="title-g__fz--64 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array1?></span>
                                        <strong class="title-g__fz--80 fontXBold colorWhite title-g__block"><?php echo $blockcomp1_array2?></strong>
                                    </h2>
                                </div>
                            <?php endif; ?>
                            <?php if(get_sub_field('texto_carrousel_bloqueg',194) ):  ?>
                                <div class="title-g__text colorWhite title-g__fz--35 fontRegular">
                                    <?php the_sub_field('texto_carrousel_bloqueg',194) ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php if(get_sub_field('imagen_carrousel_bloqueg',194)) :?>
                        <div class="block-g__image">
                            <div class="block-g__figure equipos-banner"></div>
                            <figure class="block-g__img">
                                <img src="<?php the_sub_field('imagen_carrousel_bloqueg',194) ?>" alt="">
                            </figure>
                        </div>
                        <?php endif; ?>
                    </div>
                </section> 
            <?php } ?>
            <?php if( get_sub_field('tipo_carrousel_bloqueg', 194) == 'mapa' ) { ?>
                <section class="block-g inv-list--banner inv-list--banner2">
                    <div class="block-g__container wrapper-container">
                        <div class="block-g__map">
                            <div class="block-g__figure2"></div>
                            <?php get_template_part( 'templates/equipamientos/content', 'map'); ?>
                            <img class="hand-svg" src="<?php echo get_home_url(); ?>/wp-content/themes/wp-theme-consytec/public/assets/images/hand.svg" alt="">
                        </div>  
                        <div class="title-g block-g__title">
                            <div class="block-g__mapElements">
                        <?php   
                        $custom_terms = get_terms('departamento'); ?>
                        <?php
                        foreach($custom_terms as $custom_term) {
                            wp_reset_query();
                            $args = array(
                                'post_type' => 'equipamiento',
                                'posts_per_page' => -1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'departamento',
                                        'field' => 'slug',
                                        'terms' => $custom_term->slug,
                                        
                                    ),
                                ),
                            );
                            $loop = new WP_Query($args);
                            if($loop->have_posts()) { ?>
                                <?php global $post;  $h=0; ?>
                                <?php while($loop->have_posts()) : $loop->the_post(); ?><?php $h++;?><?php endwhile;?>
                                
                                <div class="block-g__mapElement" data-id="<?php echo str_replace('-', '',$custom_term->slug); ?>">
                                    <!-- <div style="display: none;" class="closeCity">X</div> -->
                                    <div class="block-g__mapElementIn">
                                         <div class="block-g__mapEItemTitle">
                                            <h3 class="titleCount"><?php echo $custom_term->name ?>( <span <?php echo 'data-' . $custom_term->name ?> <?php echo '="' . $h . '"'; ?> > <?php echo $h ?>)</span></h3>
                                        </div>
                                        <div class="block-g__mapElementItems">
                                            <?php while($loop->have_posts()) : $loop->the_post(); ?>
                                            <a href="<?php the_permalink() ?>" class="block-g__mapElementItem">
                                                <figure class="block-g__mapElementItemThumb">
                                                    <?php the_post_thumbnail() ?>
                                                </figure>
                                                <span class="block-g__mapElementItemTitle">
                                                    <h3><?php the_title() ?></h3>
                                                </span>
                                            </a>
                                            <?php endwhile;?>
                                        </div>

                                    </div>
                                </div>
                            <?php
                            }
                        } ?>
                        </div>
                        <?php if(get_sub_field('titulo2_carrousel_bloqueg', 194) ):  ?>
                            <div class="title-g__title">
                                <?php
                                $blockcomp1_title = get_sub_field('titulo2_carrousel_bloqueg',194);
                                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                                $blockcomp1_array3 = $blockcomp1_titleArray[2];
                                $blockcomp1_array4 = $blockcomp1_titleArray[3];
                                ?>
                                <h2 class="g--uppercase">
                                    <span class="title-g__fz--64 fontXBold colorWhite"><?php echo $blockcomp1_array1?></span>
                                    <strong class="title-g__fz--80 fontXBold colorBlue"><?php echo $blockcomp1_array2?></strong>
                                    <span class="title-g__fz--64 fontXBold colorWhite"><?php echo $blockcomp1_array3?></span>
                                    <i class="title-g__fz--80 fontXBold colorWhite"><?php echo $blockcomp1_array4?></i>
                                </h2>
                            </div>
                            <?php if(get_sub_field('texto2_carrousel_bloqueg',194) ):  ?>
                                <div class="title-g__text colorWhite title-g__fz--35 fontRegular">
                                    <?php the_sub_field('texto2_carrousel_bloqueg',194) ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        </div>
                    </div>
                </section> 
                
            <?php } ?>
            </li>
<?php endwhile; ?>
<?php endif; ?>
        
    </ul>
    <div class="slider--principalPagination"></div>
</section>


<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(){
        
        let titleCount = document.getElementsByClassName('titleCount');
        let citiesCount = document.getElementById('citiesCount');
        // console.log(titleCount.length);

        function placeDiv(elem, city) {

            var rect = elem.getBoundingClientRect(); 

            var d = document.getElementById(city);

            var total_width = document.querySelector('.swiper-wrapper').offsetWidth;

            //get the middle of current elemento
                
            let middle = elem.getClientRects()[0].width / 2;

            let middle_h = elem.getClientRects()[0].height / 2;

            let difference = middle - d.offsetWidth;
            let difference_h = middle_h - d.offsetHeight;

            // console.log(rect.x);
            // console.log(difference_h);
            // console.log(difference);
            // console.log(rect.x - total_width);
            // var d = document.getElementById('yourDivId');
            d.style.position = "absolute";
            d.style.left = ( rect.x - total_width + difference  )+'px';
            d.style.top = (rect.y + difference_h )+'px';

        }


        for (var i = 0; i < titleCount.length; i++) {
            
            let dat = titleCount[i].textContent;  
            // let dat = titleCount[i];  
            // let nameCity = dat.match(/[a-zA-Z]+/g)[0];
            let nameCity = dat.replace(/[0-9]/g, '').replace(/[- )(]/g,'');;

            // console.log(nameCity);
            
            nameCity = JSON.stringify(nameCity).toLowerCase().replace(/^"|"$/g, '');

            cityName = 'city' + nameCity;


            let countCity = dat.match(/(\d+)/)[0];

            // console.log(typeof countCity);

            // console.log(nameCity + ' - ' +  countCity);

            // let cardCityCount = `<div id="${cityName}" class="circleItem" data-city="${nameCity}">
            //                         <div class="circleCount"></div>
            //                         <p>${countCity}</p>
            //                      </div>`;

            // citiesCount.innerHTML += cardCityCount;
            // console.log(nameCity);
            document.getElementById('number-' + nameCity).textContent = countCity;

            let current_city = document.getElementById('number-' + nameCity).textContent;

            console.log(nameCity + '*' + current_city);

            // if (document.getElementById('number-' + nameCity).textContent == '0') {
            //     console.log('esta vacio');
                document.getElementById('circle-' + nameCity).style.display = 'block';
                document.getElementById('number-' + nameCity).style.display = 'block';

            // }



            // let get = document.getElementById('lima');

            // placeDiv( document.getElementById(nameCity), cityName );


        }
});



/****

1. Completear l id del mapa entero
2. Pintar los ciruclos
3. Probar function que anine a los ciruclos al medio

4. Hacer que se ejecute




***/











</script>







<div id="citiesCount">
    
</div>



<style>
#cityCount {position: absolute;
     /*left: 396.01px;*/
     /*top: 661.057px;*/
     z-index: 999 !important;
     font-size: 25px;
     color: red;   } 


.circleItem {
    position: relative;
    width: 40px;
    height: 40px;
    text-align: center;
    display: flex;
    z-index: 9999 !important;
    align-items: center;
    vertical-align: middle;
    color: #fff;
    font-weight: bold;
    justify-content: space-around; 
    cursor: pointer;
}

.circleItem p {
    margin: 0 !important;
}

.circleCount {
    width: 40px;
    height: 40px;
    /* border: solid red; */
    border-radius: 50%;
    position: absolute;
    background-repeat: no-repeat;
    background-image: url(../../wp-content/themes/wp-theme-consytec/public/assets/images/3.4.png);
    background-size: contain;
}


</style>

<script>

    document.querySelector('.closeCity').addEventListener('click', function(){

        // alert('close');
        document.querySelector('.block-g__mapElements').style.display = 'none';

    });




    // let getCity = document.getElementById('lima');

    // let cityCount = document.getElementById('cityCount');
    
// function getPositionXY(element) { 
    // var rect = element.getBoundingClientRect(); 

    // var total_width = document.querySelector('.swiper-wrapper').offsetWidth;

    // console.log('X: ' + rect.x + ' - ' + 'Y: ' + rect.y );
// }
// getPositionXY(getCity);



// d.style.position = "absolute";
// d.style.left = x_pos+'px';
// d.style.top = y_pos+'px';


// placeDiv(getCity);


</script>