
<?php  
$taxonomy     = 'categoria-equipamiento';
$orderby      = 'ASC'; 
$show_count   = false;
$pad_counts   = false;
$hierarchical = true;
$title        = '';

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title
);
?>

<div class="g-menu__categories list--none">
    <ul>
        <?php wp_list_categories( $args ); ?>
    </ul>
</div>
