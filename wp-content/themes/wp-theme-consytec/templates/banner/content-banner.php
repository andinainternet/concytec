<?php if( get_field('activar_header_section') ): ?>
    <div class="block-top">
        <div class="block-top__container wrapper-container">
            <?php if( get_field('imagen_header_section') ): ?>
                
            <?php endif ?>
            <figure class="block-top__image">
                <img src="<?php the_field('imagen_header_section') ?>" alt="">
            </figure>
            <div class="block-top__info">
            <?php if( get_field('titulo_header_section') ): ?>
                <div class="block-top__title">
                    <h1><?php the_field('titulo_header_section') ?></h1>
                </div>
            <?php endif ?>
            <?php if( get_field('texto_header_section') ): ?>
                <div class="block-top__text">
                    <?php the_field('texto_header_section') ?>
                </div>
            <?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>