<?php
/* Template Name: Template - Blog*/
get_header(); ?>


<!-- Outstanding-->
<?php get_template_part( 'templates/blog/content', 'outstanding'); ?>

<!-- News -->
<div class="news-block">
    <div class="news-block__container wrapper-container">
        <div class="news-listPage">
            <!-- News List-->
            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
            <?php
            $args = array( 
            'post_type' => 'post', 
            'post_status' => 'publish',
            'posts_per_page' => '5',
            'paged' => $paged,
            );
            $wp_query = new WP_Query($args); ?>
            <?php if($wp_query->have_posts()) : ?>
            <div class="news-list">
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <?php get_template_part( 'templates/blog/content', 'list'); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
             <!-- News Pagination-->
            <div class="news-list__pagination">
            <?php get_template_part( 'templates/pagination/content', 'pagination'); ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="news-SearchrecentCategories">
            <?php get_template_part( 'templates/blog/content', 'aside'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
