<?php
/* Template Name: Template - Video */
get_header(); ?>


<section class="header-single">
	<div class="bg-single bg-single-video" style="background-image: url(<?php echo get_field( 'banner_video' ); ?>)">
		<h1 class="titleSingle title-g__fz--55 fontXBold title-g__block"><?php echo get_the_title(); ?></h1>
	</div>
</section>




<section class="wrapper-padding wrapper-video">
	
	<div class="wrapper-fullwidth">


		<div class="wrapper-grid">

		<?php if( have_rows('contenido_video') ): ?>
		<?php while( have_rows('contenido_video') ): the_row(); ?>

			<!-- START LOOP -->
			<?php 
				$componente_bg_color = get_sub_field( 'componente_bg_color' );
			 ?>

			<section class="block-g block-g--component1 bg-color-<?php echo $componente_bg_color; ?>" id="componente1">
			    
			    <div class="block-g__container wrapper-container">  
			        <div class="title-g block-g__title">
			            <div class="title-g__title">
			    
			                <?php if(get_sub_field('titulo_componente1') ):  ?>

			                <?php
			                    $blockcomp1_title = get_sub_field('titulo_componente1');
			                    // $blockcomp1_title = 'Encuentros y seminarios';

			                    $blockcomp1_title = get_sub_field('titulo_componente1');
								// $blockcomp1_subtitle = 'Espacios de socialización, conocimiento y sinergias';

			                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
			                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
			                    $blockcomp1_array2 = $blockcomp1_titleArray[1];
			                    ?>
			                    <h2 class="g--uppercase">

			                    	<?php 
			                    		if ($componente_bg_color == 'blanco'){
			                    			$colorT = 'Blue';
			                    		} else {
			                    			$colorT = 'White';

			                    		} ?>

			                        <strong class="title-g__fz--70 fontXBold color<?php echo $colorT; ?> title-g__block"><?php echo $blockcomp1_array1?></strong>
			                        <span class="title-g__fz--36 fontBold color<?php echo $colorT; ?> title-g__block"><?php echo $blockcomp1_array2?></span>
			                    </h2>
			                <?php endif; ?>
			                <?php if(get_sub_field('subtitulo_componente1') ):  ?>
			                    <h3>
			                        <span style="line-height: 1.2;font-family: 'Montserrat';font-weight: 600; width: 78%; margin: 0;" class="title-g__fz--32 fontBold color<?php echo $colorT; ?> title-g__block"><?php the_sub_field('subtitulo_componente1')?></span>
			                    </h3>
			                <?php endif; ?>
			            </div>

			            <?php
			                $url_youtubeLst = get_sub_field('componente_componente1'); ?>
			                <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
			                    <?php the_content(); ?>
			                </div>
			                <div class="title-g__buttons">
			                    <div class="title-g__button">
			                        <a class="button-g button-g--lightBlue2 g--uppercase" href="<?php echo $url_youtubeLst; ?>">
			                            <span class="button-g__text btn-videos"><?php _e('Ver videos','concytec')?></span>
			                        </a>
			                    </div>
			                </div>
			                <?php wp_reset_postdata(); ?>
			        </div>
			        <div class="block-g__image">
			            <div class="block-g__figure figure_bg_video"></div>
			            <?php
			                $image_relational = get_sub_field('componente_image'); ?>
			                <figure class="block-g__img">
			                    <!-- <?php //the_post_thumbnail() ?> -->
			                    <img src="<?php echo $image_relational; ?>" alt="">
			                </figure>
			                <?php wp_reset_postdata(); ?>
			        </div>
			    </div>
			</section>

			<!-- ENDS LOOP -->

		<?php endwhile; ?>
		<?php endif; ?>			

		</div>



	</div>

</section>


<?php get_template_part( 'templates/blog/content', 'blog'); ?>
<?php get_footer(); ?>