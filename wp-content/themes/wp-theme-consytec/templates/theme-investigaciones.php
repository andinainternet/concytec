<?php
/* Template Name: Template - Investigaciones */
get_header(); ?>


<?php if( have_rows('equipamiento_contenido') ): ?>
<?php while( have_rows('equipamiento_contenido') ): the_row(); ?>
<?php if( get_row_layout() == 'banner' ): ?>
<!-- Banner -->
<section class="block-g equipamiento-block--banner">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title">
            <div class="title-g__title">
                    <h1 class="g--uppercase">
                        <?php if(get_sub_field('titulo_1_equipamiento_pagina') ):  ?>
                            <span class="title-g__fz--56 fontBold colorWhite title-g__block"><?php the_sub_field('titulo_1_equipamiento_pagina') ?></span>
                        <?php endif; ?>
                        <?php if(get_sub_field('titulo_2_equipamiento_pagina') ):  ?>
                            <strong class="title-g__fz--138 fontXBold colorGreen title-g__block"><?php the_sub_field('titulo_2_equipamiento_pagina')  ?></strong>
                        <?php endif; ?>
                        <?php if(get_sub_field('titulo_3_equipamiento_pagina') ):  ?>
                            <i class="title-g__fz--80 fontXBold colorLightBlue2 title-g__block"><?php the_sub_field('titulo_3_equipamiento_pagina')  ?></i>
                        <?php endif; ?>
                    </h1>
            </div>
                <?php if(get_sub_field('texto_equipamiento_pagina') ):  ?>
                <div class="title-g__text colorWhite fontMedium titleText-g__fz--35">
                    <?php the_sub_field('texto_equipamiento_pagina') ?>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('boton_url_equipamiento_pagina') ):  ?>
                <div class="title-g__buttons">
                    <div class="title-g__button">
                        <a class="button-g button-g--green g--uppercase" href="<?php the_sub_field('boton_url_equipamiento_pagina') ?>">
                            <span class="button-g__text"><?php the_sub_field('boton_label_equipamiento_pagina') ?></span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_equipamiento_pagina')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_equipamiento_pagina') ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section>  

<?php elseif( get_row_layout() == 'desarrollo' ):  ?>
<!-- Desarrollo -->


<section class="block-g equipamiento-block--fort">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title">
            <div class="title-g__title">
                <?php
                $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina2');
                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                $blockcomp1_array3 = $blockcomp1_titleArray[2];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--64 fontBold colorWhite"><?php echo $blockcomp1_array1?></span>
                    <strong class="title-g__fz--80 fontXBold colorWhite"><?php echo $blockcomp1_array2?></strong>
                    <span class="title-g__fz--64 fontBold colorWhite"><?php echo $blockcomp1_array3?></span>
                </h2>
            </div>
                <?php if(get_sub_field('texto_equipamiento_pagina2') ):  ?>
                <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina2') ?>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('boton_url_equipamiento_pagina') ):  ?>
                <div class="title-g__buttons">
                    <div class="title-g__button">
                        <a class="button-g button-g--green g--uppercase" href="<?php the_sub_field('boton_url_equipamiento_pagina') ?>">
                            <span class="button-g__text"><?php the_sub_field('boton_label_equipamiento_pagina') ?></span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_equipamiento_pagina2')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_equipamiento_pagina2') ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section>  

<?php elseif( get_row_layout() == 'descentralizacion' ):  ?>


    <section class="equipamiento-block__inf equipamiento-block__inf--inv">
    <span class="equipamiento-block-infFigure"></span>
    <div class="equipamiento-block__infContainer wrapper-container">
        <div class="title-g equipamiento-block__infTitleTop">
            <?php if(get_sub_field('titulo_equipamiento_pagina3')) :?>
                <div class="title-g__title">
                    <?php
                    $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina3');
                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
                    ?>
                    <h2 class="g--uppercase">
                        <span class="title-g__fz--64 fontXBold colorBlue"><?php echo $blockcomp1_array1?></span>
                    </h2>
                </div>
            <?php endif; ?>
        </div>
        <div class="equipamiento-block__infMapContent">
                <figure class="equipamiento-block__infMap">
                <?php if(get_sub_field('titulo_equipamiento_pagina3')) :?>
                    <img src="<?php the_sub_field('imagen_equipamiento_pagina3') ?>" alt="">
                <?php endif; ?>
                <?php if( have_rows('cuadro_equipamiento_pagina3') ): ?>
                <?php while( have_rows('cuadro_equipamiento_pagina3') ): the_row(); ?>
                    <div class="equipamiento-block__infMapSquare">
                        <span><?php the_sub_field('texto1_cuadro_equipamiento_pagina3') ?></span>
                        <span><?php the_sub_field('texto2_cuadro_equipamiento_pagina3') ?></span>
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>
                </figure>
            
            <div class="equipamiento-block__infContent">
                <div class="title-g equipamiento-block__infTitle">
                    <?php if(get_sub_field('texto_equipamiento_pagina3') ):  ?>
                        <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                            <?php the_sub_field('texto_equipamiento_pagina3') ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if(get_sub_field('tabla_equipamiento_pagina3')): ?>
                    <div class="equipamiento-block__infTable">
                        <div class="table-g">
                        <?php
                        $table = get_sub_field( 'tabla_equipamiento_pagina3' );

                        if ( ! empty ( $table ) ) {
                        
                            echo '<table border="0">';
                        
                                if ( ! empty( $table['caption'] ) ) {
                        
                                    echo '<caption>' . $table['caption'] . '</caption>';
                                }
                        
                                if ( ! empty( $table['header'] ) ) {
                        
                                    echo '<thead>';
                        
                                        echo '<tr>';
                        
                                            foreach ( $table['header'] as $th ) {
                        
                                                echo '<th>';
                                                    echo $th['c'];
                                                echo '</th>';
                                            }
                        
                                        echo '</tr>';
                        
                                    echo '</thead>';
                                }
                        
                                echo '<tbody>';
                        
                                    foreach ( $table['body'] as $tr ) {
                        
                                        echo '<tr>';
                        
                                            foreach ( $tr as $td ) {
                        
                                                echo '<td>';
                                                    echo $td['c'];
                                                echo '</td>';
                                            }
                        
                                        echo '</tr>';
                                    }
                        
                                echo '</tbody>';
                        
                            echo '</table>';
                        }
                        ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'conocimiento' ):  ?>

    <section class="equipamiento-block__sp">
    <div class="equipamiento-block__spContainer wrapper-container">
        <div class="equipamiento-block__spPercent">
        <?php if( have_rows('financiamiento_equipamiento_pagina4') ): ?>
        <?php while( have_rows('financiamiento_equipamiento_pagina4') ): the_row(); ?>
            <div class="equipamiento-block__spPercentTitle">
                <h3><?php the_sub_field('titulo_financiamiento_equipamiento_pagina3') ?></h3>
                <span><?php the_sub_field('monto_financiamiento_equipamiento_pagina3') ?></span>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
    
        <?php if( have_rows('porcentajes_equipamiento_pagina4') ): ?>
            <div class="equipamiento-block__spPercentAllList">
                <div class="equipamiento-block__spPercentAll">
                    <?php while( have_rows('porcentajes_equipamiento_pagina4') ): the_row(); ?>
                        <?php
                            $numeroData[] = get_sub_field('numero_porcentajes_equipamiento_pagina3');
                            $colorData[] = get_sub_field('color_porcentajes_equipamiento_pagina3');
                        ?>
                    <?php endwhile; ?>

                    <?php 
                        $numeroDataJson = json_encode($numeroData);
                        $colorDataJson = json_encode($colorData);
                    ?>
                    
                    <canvas id="doughnut-conocimiento" width="362" height="362"></canvas>

                    <script>
                        new Chart(document.getElementById("doughnut-conocimiento"), {
                        type: 'doughnut',
                        data: {
                            datasets: [
                            {
                                borderColor: '#005470',
                                borderWidth: 1,
                                backgroundColor: JSON.parse('<?php echo $colorDataJson ?>'),
                                data: JSON.parse('<?php echo $numeroDataJson ?>')
                            }
                          ]
                        },
                        options: {
                            title: {
                                display: false
                            }
                        }
                    });
                    </script>   
                   
                </div>
                <div class="equipamiento-block__spPercentList">
                <?php while( have_rows('porcentajes_equipamiento_pagina4') ): the_row(); ?>
                    <article class="equipamiento-block__spPercentItem">
                        <h4 style="color:<?php the_sub_field('color_porcentajes_equipamiento_pagina3') ?>">
                            <span style="background-color:<?php the_sub_field('color_porcentajes_equipamiento_pagina3') ?>"></span>
                            <?php the_sub_field('numero_porcentajes_equipamiento_pagina3') ?>%
                            <?php the_sub_field('numeron_porcentajes_equipamiento_pagina3') ?>
                            <?php if( get_sub_field('financiamiento_porcentajes_equipamiento_pagina3') ): ?>
                                - <?php the_sub_field('financiamiento_porcentajes_equipamiento_pagina3') ?>
                            <?php endif; ?>
                        </h4>
                        <span><?php the_sub_field('texto_porcentajes_equipamiento_pagina3') ?></span>
                    </article>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
        <div class="title-g equipamiento-block__spTitle">
            <?php if(get_sub_field('titulo_equipamiento_pagina4') ):  ?>
                <div class="title-g__title">
                    <?php
                    $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina4');
                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
                    $blockcomp1_array2 = $blockcomp1_titleArray[1];
                    ?>
                    <h2 class="g--uppercase">
                        <span class="title-g__fz--64 fontBold colorWhite"><?php echo $blockcomp1_array1?></span>
                        <strong class="title-g__fz--80 fontXBold colorWhite"><?php echo $blockcomp1_array2?></strong>
                    </h2>
                </div>
            <?php endif; ?>
            <?php if(get_sub_field('texto_equipamiento_pagina4') ):  ?>
                <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina4') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'sectores' ):  ?>


<section class="inv-block__sectors">
    <div class="inv-block__sectorsContainer wrapper-container">
        <span class="inv-block__sectorsFigure"></span>
        <div class="title-g inv-block__sectorsTitle">
            <?php if(get_sub_field('titulo_equipamiento_pagina5') ):  ?>
                <div class="title-g__title">
                    <?php
                    $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina5');
                    $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                    $blockcomp1_array1 = $blockcomp1_titleArray[0];
                    $blockcomp1_array2 = $blockcomp1_titleArray[1];
                    ?>
                    <h2 class="g--uppercase">
                        <span class="title-g__fz--64 fontXBold colorBlue title-g__block"><?php echo $blockcomp1_array1?></span>
                        <strong class="title-g__fz--64 fontXBold colorBlue title-g__block"><?php echo $blockcomp1_array2?></strong>
                    </h2>
            </div>
            <?php endif; ?>
            <?php if(get_sub_field('texto_equipamiento_pagina5') ):  ?>
                <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina5') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="inv-block__sectorsTables">
            <div class="inv-block__sectorsTable">
                <div class="table-g">
                    <?php
                    $table = get_sub_field( 'tabla_equipamiento_pagina5' );

                    if ( ! empty ( $table ) ) {
                    
                        echo '<table border="0">';
                    
                            if ( ! empty( $table['caption'] ) ) {
                    
                                echo '<caption>' . $table['caption'] . '</caption>';
                            }
                    
                            if ( ! empty( $table['header'] ) ) {
                    
                                echo '<thead>';
                    
                                    echo '<tr>';
                    
                                        foreach ( $table['header'] as $th ) {
                    
                                            echo '<th>';
                                                echo $th['c'];
                                            echo '</th>';
                                        }
                    
                                    echo '</tr>';
                    
                                echo '</thead>';
                            }
                    
                            echo '<tbody>';
                    
                                foreach ( $table['body'] as $tr ) {
                    
                                    echo '<tr>';
                    
                                        foreach ( $tr as $td ) {
                    
                                            echo '<td>';
                                                echo $td['c'];
                                            echo '</td>';
                                        }
                    
                                    echo '</tr>';
                                }
                    
                            echo '</tbody>';
                    
                        echo '</table>';
                    }
                    ?>
                </div>
            </div>
            <div class="inv-block__sectorsTable">
                <div class="table-g">
                    <?php
                    $table = get_sub_field( 'tabla2_equipamiento_pagina5' );

                    if ( ! empty ( $table ) ) {
                    
                        echo '<table border="0">';
                    
                            if ( ! empty( $table['caption'] ) ) {
                    
                                echo '<caption>' . $table['caption'] . '</caption>';
                            }
                    
                            if ( ! empty( $table['header'] ) ) {
                    
                                echo '<thead>';
                    
                                    echo '<tr>';
                    
                                        foreach ( $table['header'] as $th ) {
                    
                                            echo '<th>';
                                                echo $th['c'];
                                            echo '</th>';
                                        }
                    
                                    echo '</tr>';
                    
                                echo '</thead>';
                            }
                    
                            echo '<tbody>';
                    
                                foreach ( $table['body'] as $tr ) {
                    
                                    echo '<tr>';
                    
                                        foreach ( $tr as $td ) {
                    
                                            echo '<td>';
                                                echo $td['c'];
                                            echo '</td>';
                                        }
                    
                                    echo '</tr>';
                                }
                    
                            echo '</tbody>';
                    
                        echo '</table>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'investigacion_aplicada' ):  ?>

<section class="inv-block__inva">
    <div class="inv-block__invaContainer wrapper-container">
        <div class="title-g inv-block__invaTitle">
            <?php if(get_sub_field('titulo_equipamiento_pagina6') ):  ?>
                <div class="title-g__title">
                    <h2 class="title-g__fz--64 fontXBold colorWhite g--uppercase">
                        <?php the_sub_field('titulo_equipamiento_pagina6')?>
                    </h2>
            </div>
            <?php endif; ?>
            <?php if(get_sub_field('texto_equipamiento_pagina6') ):  ?>
                <div class="title-g__text colorWhite fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina6') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="inv-block__invaTable">
            <div class="table-g">
                <?php
                $table = get_sub_field( 'tabla_equipamiento_pagina6' );

                if ( ! empty ( $table ) ) {
                
                    echo '<table border="0">';
                
                        if ( ! empty( $table['caption'] ) ) {
                
                            echo '<caption>' . $table['caption'] . '</caption>';
                        }
                
                        if ( ! empty( $table['header'] ) ) {
                
                            echo '<thead>';
                
                                echo '<tr>';
                
                                    foreach ( $table['header'] as $th ) {
                
                                        echo '<th>';
                                            echo $th['c'];
                                        echo '</th>';
                                    }
                
                                echo '</tr>';
                
                            echo '</thead>';
                        }
                
                        echo '<tbody>';
                
                            foreach ( $table['body'] as $tr ) {
                
                                echo '<tr>';
                
                                    foreach ( $tr as $td ) {
                
                                        echo '<td>';
                                            echo $td['c'];
                                        echo '</td>';
                                    }
                
                                echo '</tr>';
                            }
                
                        echo '</tbody>';
                
                    echo '</table>';
                }
                ?>
            </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'investigaciones' ):  ?>

<section class="block-g equipamientos-block--equipamientos">
    <div class="block-g__container wrapper-container">  
        <div class="title-g block-g__title">
            <div class="title-g__title">
                <?php
                $blockcomp1_title = get_sub_field('titulo_equipamiento_pagina7');
                $blockcomp1_titleArray = preg_split( "/[|]/", $blockcomp1_title );
                $blockcomp1_array1 = $blockcomp1_titleArray[0];
                $blockcomp1_array2 = $blockcomp1_titleArray[1];
                $blockcomp1_array3 = $blockcomp1_titleArray[2];
                ?>
                <h2 class="g--uppercase">
                    <span class="title-g__fz--64 fontXBold colorBlue"><?php echo $blockcomp1_array1?></span>
                    <strong class="title-g__fz--80 fontXBold colorBlue title-g__block"><?php echo $blockcomp1_array2?></strong>
                    <span class="title-g__fz--64 fontXBold colorBlue"><?php echo $blockcomp1_array3 ?></span>
                </h2>
            </div>
                <?php if(get_sub_field('texto_equipamiento_pagina7') ):  ?>
                <div class="title-g__text colorBlue fontRegular titleText-g__fz--24">
                    <?php the_sub_field('texto_equipamiento_pagina7') ?>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('boton_url_equipamiento_pagina7') ):  ?>
                <div class="title-g__buttons">
                    <div class="title-g__button">
                        <a class="button-g button-g--green g--uppercase" href="<?php the_sub_field('boton_url_equipamiento_pagina7') ?>">
                            <span class="button-g__text"><?php the_sub_field('boton_label_equipamiento_pagina7') ?></span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
        </div>
        <?php if(get_sub_field('imagen_equipamiento_pagina7')) :?>
        <div class="block-g__image">
            <div class="block-g__figure"></div>
            <figure class="block-g__img">
                <img src="<?php the_sub_field('imagen_equipamiento_pagina7') ?>" alt="">
            </figure>
        </div>
        <?php endif; ?>
    </div>
</section>

<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>




<!--Blog-->
<?php get_template_part( 'templates/blog/content', 'blog'); ?>

<?php get_footer(); ?>